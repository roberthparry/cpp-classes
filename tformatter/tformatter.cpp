/*
 *  Name:           tformatter.cpp
 *  Copyright:      BB&D
 *  Author:         R. Parry
 *  Date:           22/12/03
 *  Description: 
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "tformatter.h"
/*--------------------------------------------------------------------------------------------------
   tFormatter::GetSection - returns a pointer to the section structure corresponding to the
                            required section specified by eSection. NULL return means error.
  --------------------------------------------------------------------------------------------------*/
tFormatter::tSection* tFormatter::GetSection( tFormatter::ESection eSection ) const
{
   tFormatter::tSection* pSection = NULL;
   switch ( m_nSections ) {
      // One section: positive, negative & zero.
   case 1:
      pSection = &m_pSections[ 0 ];
      break;
      // Two sections: positive & zero ; negative.
   case 2:
      pSection = ( eSection == tFormatter::NegativeSection ) ? &m_pSections[ 1 ] : &m_pSections[ 0 ];
      break;
      // Three sections: positive ; negative ; zero
   case 3:
      switch ( eSection ) {
      case tFormatter::PositiveSection:
         pSection = &m_pSections[ 0 ];
         break;
      case tFormatter::NegativeSection:
         pSection = &m_pSections[ 1 ];
         break;
      case tFormatter::ZeroSection:
         pSection = &m_pSections[ 2 ];
         break;
      default:
         break;
      }
      break;
   default:
      break;
   }
   return pSection;
}
/*--------------------------------------------------------------------------------------------------
   tFormatter::GetSection - gets the section of the format pattern that is appropriate to the sign
                            of dValue.
  --------------------------------------------------------------------------------------------------*/
tFormatter::tSection* tFormatter::GetSection( double dValue ) const
{
   if ( dValue == 0.0 )
      return GetSection( tFormatter::ZeroSection );
   else if ( dValue < 0.0 )
      return GetSection( tFormatter::NegativeSection );
   else
      return GetSection( tFormatter::PositiveSection );
}
/*--------------------------------------------------------------------------------------------------
   tFormatter::CompleteSection - fill in the remaining fields for the section based on the pattern,
                                 decimal and exponent place-holders.
  --------------------------------------------------------------------------------------------------*/
bool tFormatter::CompleteSection( tFormatter::tSection* pSection )
{
   char* pchPattern;
   char* pchLastDigit = NULL;
   char  chPrevious = '\0';
   bool  fLiteral = false;
   if ( pSection == NULL )
      return false;
   pSection->dScale = 1.0;
   // Determine the decimal digit requirements
   if ( pSection->pchDecimal ) {
      unsigned& nOptionalDecDigits = pSection->nOptionalDecDigits;
      unsigned& nMandatoryDecDigits = pSection->nMandatoryDecDigits;
      unsigned nThousands = 0U;
      for ( pchPattern = &pSection->pchDecimal[ 1 ]; *pchPattern; chPrevious = *pchPattern, pchPattern ++ )
      {
         if ( pchPattern == pSection->pchExponent )
            break;
         char chCurrent = *pchPattern;
         if ( chPrevious == '\\' )
            continue;
         if ( chCurrent == '\"' ) {
            fLiteral = ( fLiteral ) ? false : true;
            continue;
         }
         if ( fLiteral )
            continue;
         if ( chCurrent == '#' || chCurrent == '0' ) {
            if ( nThousands )
               nThousands = 0U;
            nOptionalDecDigits ++;
            if ( chCurrent == '0' )
               nMandatoryDecDigits = nOptionalDecDigits;
            if ( pchLastDigit < pchPattern )
               pchLastDigit = pchPattern;
         }
         if ( chCurrent == ',' )
            nThousands ++;
      }
      for ( unsigned uiThousand = 0U; uiThousand < nThousands; uiThousand ++ )
         pSection->dScale /= 1000.0;
   }
   // Determine the mantissa digit requirements
   unsigned& nMantissaDigits = pSection->nMantissaDigits;
   unsigned  nDigits = 0U;
   char* pchMantPatternEnd;
   if ( pSection->pchDecimal )
      pchMantPatternEnd = pSection->pchDecimal;
   else if ( pSection->pchExponent )
      pchMantPatternEnd = pSection->pchExponent;
   else
      pchMantPatternEnd = pSection->pchSection + strlen( pSection->pchSection );
   fLiteral = false;
   bool fDigits = false;
   for ( pchPattern = &pchMantPatternEnd[ -1 ]; pchPattern >= pSection->pchSection; pchPattern -- )
   {
      if ( pchPattern > pSection->pchSection && pchPattern[ -1 ] == '\\' )
         continue;
      char chCurrent = *pchPattern;
      if ( chCurrent == '\"' ) {
         fLiteral = ( fLiteral ) ? false : true;
         continue;
      }
      if ( fLiteral )
         continue;
      if ( chCurrent == '#' || chCurrent == '0' ) {
         fDigits = true;
         nDigits ++;
         if ( chCurrent == '0' )
            nMantissaDigits = nDigits;
         if ( pchLastDigit < pchPattern )
            pchLastDigit = pchPattern;
      }
      if ( chCurrent == ',' ) {
         if ( pSection->pchDecimal )
            pSection->fThousandsSep = true;
         else if ( ! fDigits )
            pSection->dScale /= 1000.0;
      }
   }
   // determine the exponent digit requirements.
   if ( pSection->pchExponent ) {
      unsigned& nExponentDigits = pSection->nExponentDigits;
      nDigits = 0U;
      chPrevious = '\0';
      fLiteral = false;
      for ( pchPattern = &pSection->pchExponent[ 1 ]; *pchPattern != '\0'; chPrevious = *pchPattern, pchPattern ++ )
      {
         char chCurrent = *pchPattern;
         if ( chPrevious == '\\' )
            continue;
         if ( chCurrent == '\"' ) {
            fLiteral = ( fLiteral ) ? false : true;
            continue;
         }
         if ( fLiteral )
            continue;
         if ( chCurrent == '#' || chCurrent == '0' ) {
            nDigits ++;
            if ( chCurrent == '0' )
               nExponentDigits = nDigits;
            if ( pchLastDigit < pchPattern )
               pchLastDigit = pchPattern;
         }
      }
   }
   // determine whether there is a percent - start from the last digit (exponent inclusive)
   if ( pchLastDigit == NULL )
      return true;
   chPrevious = '\0';
   fLiteral = false;
   for ( pchPattern = &pchLastDigit[ 1 ]; *pchPattern != '\0'; chPrevious = *pchPattern, pchPattern ++ )
   {
      if ( pchPattern == pSection->pchExponent )
         break;
      char chCurrent = *pchPattern;
      if ( chPrevious == '\\' )
         continue;
      if ( chCurrent == '\"' ) {
         fLiteral = ( fLiteral ) ? false : true;
         continue;
      }
      if ( fLiteral )
         continue;
      if ( chCurrent == '%' ) {
         pSection->fPercent = true;
         pSection->dScale *= 100.0;
         break;
      }
   }
   return true;
}
/*--------------------------------------------------------------------------------------------------
   tFormatter::LocalCopy
  --------------------------------------------------------------------------------------------------*/
void tFormatter::LocalCopy( const tFormatter& fmt )
{
   m_pszPattern = strdup( fmt.m_pszPattern );
   m_nSections = fmt.m_nSections;
   m_pSections = new tFormatter::tSection[ m_nSections ];
   memcpy( m_pSections, fmt.m_pSections, m_nSections * sizeof( tFormatter::tSection ) );
   int cbPatternOffset = (int) (m_pszPattern - fmt.m_pszPattern);
   for ( unsigned uiSection = 0U; uiSection < m_nSections; uiSection ++ ) {
      tFormatter::tSection& Section = m_pSections[ uiSection ];
      Section.pchDecimal += cbPatternOffset;
      Section.pchExponent += cbPatternOffset;
      Section.pchSection += cbPatternOffset;
   }
}
/*--------------------------------------------------------------------------------------------------
   tFormatter::LocalDelete
  --------------------------------------------------------------------------------------------------*/
void tFormatter::LocalDelete()
{
   if ( m_pszPattern )
      SetPattern( NULL );
   if ( m_pSections ) {
      delete [] m_pSections;
      m_pSections = NULL;
   }
}
/*--------------------------------------------------------------------------------------------------
   tFormatter ctor
  --------------------------------------------------------------------------------------------------*/
tFormatter::tFormatter( const char* pszPattern /*= NULL*/ )
      :  m_nSections    ( 0U )
      ,  m_pSections    ( NULL )
      ,  m_pszPattern   ( NULL )
{
   if ( pszPattern )
      SetPattern( pszPattern );
}
/*--------------------------------------------------------------------------------------------------
   tFormatter - dtor
  --------------------------------------------------------------------------------------------------*/
tFormatter::~tFormatter()
{
   LocalDelete();
}
/*--------------------------------------------------------------------------------------------------
   tFormatter - cctor
  --------------------------------------------------------------------------------------------------*/
tFormatter::tFormatter( const tFormatter& fmt )
{
   LocalCopy( fmt );
}
/*--------------------------------------------------------------------------------------------------
   tFormatter::operator=
  --------------------------------------------------------------------------------------------------*/
tFormatter& tFormatter::operator=( const tFormatter& fmt )
{
   if ( this != &fmt ) {
      LocalDelete();
      LocalCopy( fmt );
   }
   return *this;
}
/*--------------------------------------------------------------------------------------------------
   tFormatter::SetPattern - Deletes the old pattern and sets a new pattern. If pszPattern is NULL
                            the current pattern is cleared and the function exits.
                            Returns false if the pattern is not valid.
  --------------------------------------------------------------------------------------------------*/
bool tFormatter::SetPattern( const char* pszPattern )
{
   if ( m_pszPattern )
      free( m_pszPattern );
   if ( pszPattern == NULL ) {
      m_pszPattern = NULL;
      return true;
   }
   m_pszPattern = strdup( pszPattern );
   m_nSections = 0U;
   bool fLiteral = false;
   char chPrevious = '\0';
   tFormatter::tSection aSections[ 3 ];
   memset( &aSections[0], 0, 3*sizeof( tFormatter::tSection ) );
   aSections[ m_nSections ].pchSection = m_pszPattern;
   for ( char* pchPattern = m_pszPattern; *pchPattern; chPrevious = *pchPattern, pchPattern ++ )
   {
      char chCurrent = *pchPattern;
      if ( chPrevious == '\\' )
         continue;
      if ( chCurrent == '\"' ) {
         fLiteral = ( fLiteral ) ? false : true;
         continue;
      }
      if ( fLiteral )
         continue;
      if ( chCurrent == '.' ) {
         aSections[ m_nSections ].pchDecimal = pchPattern;
      }
      if ( toupper( chCurrent ) == 'E' && ( pchPattern[1] == '+' || pchPattern[1] == '-' ) ) {
         aSections[ m_nSections ].pchExponent = pchPattern;
      }
      if ( chCurrent == ';' ) {
         *pchPattern = '\0';
         aSections[ ++m_nSections ].pchSection = &pchPattern[ 1 ];
      }
   }
   if ( m_pSections ) delete [] m_pSections;
   m_pSections = new tFormatter::tSection[ ++m_nSections ];
   for ( unsigned uiSection = 0U; uiSection < m_nSections; uiSection ++ ) {
      memcpy( &m_pSections[ uiSection ], &aSections[ uiSection ], sizeof( tFormatter::tSection ) );
      if ( ! CompleteSection( &m_pSections[ uiSection ] ) )
         return false;
   }
   return true;
}
/*--------------------------------------------------------------------------------------------------
   InsertChars - insert n chars of ch into pchBuffer.
  --------------------------------------------------------------------------------------------------*/
static void InsertChars( char* pszBuffer, char ch, unsigned n, int nszBuffer = -1 )
{
   char* pchBuffer = pszBuffer;
   if ( *pchBuffer ) {
      if ( nszBuffer == -1 )
         nszBuffer = strlen( pchBuffer );
      memmove( pchBuffer + n, pchBuffer, nszBuffer + 1 );
      for ( unsigned ui = 0U; ui < n; ui ++ )
         *pchBuffer ++ = ch;
   }
   else {
      for ( unsigned ui = 0U; ui < n; ui ++ )
         *pchBuffer ++ = ch;
      *pchBuffer = '\0';
   }
}
/*--------------------------------------------------------------------------------------------------
   InsertThousandsSep - insert thousands separator in pchDigits.
  --------------------------------------------------------------------------------------------------*/
static void InsertThousandsSep( char* pchDigits )
{
   char* pchStart = pchDigits;
   char* pch = &pchDigits[ strlen( pchDigits ) ];
   int nch = 3;
   while ( (pch -= 3) > pchStart && *pch != '-' ) {
      InsertChars( pch, ',', 1U, nch );
      nch += 4;
   }
}
/*--------------------------------------------------------------------------------------------------
   StrReverse - reverse the contents of psz.
  --------------------------------------------------------------------------------------------------*/
static char* StrReverse( char* psz )
{
   for ( char* pchLeft = psz, *pchRight = &pchLeft[ strlen( psz ) - 1U ];
         pchRight > pchLeft;
         pchRight --, pchLeft ++ )
   {
      char chLeft = *pchLeft;
      *pchLeft = *pchRight;
      *pchRight = chLeft;
   }
   // take care of the literal marker.
   for ( char* pch = psz; *pch; pch ++ ) {
      if ( *pch == '\\' && pch > psz )
      {
         *pch = pch[ -1 ];
         pch[ -1 ] = '\\';
      }
   }
   return psz;
}
/*--------------------------------------------------------------------------------------------------
   OutputPattern - outputs the digits pchDigits in the pattern, pchPattern and returns a pointer to
                   the next character in pchBuffer.
  --------------------------------------------------------------------------------------------------*/
char* tFormatter::OutputPattern( char* pchBuffer, char* pchDigits, char* pchPattern,
                                 bool fMantissa ) const
{
   char chPrevious = '\0';
   bool fLiteral = false;
   // as long as there are not yet any digit characters, output to buffer.
   for ( ; *pchPattern; chPrevious = *pchPattern, pchPattern ++ ) {
      char chCurrent = *pchPattern;
      if ( chCurrent == '\"' ) {
         fLiteral = ( fLiteral ) ? false : true;
         continue;
      }
      if ( chCurrent == '\\' )
         continue;
      if ( chPrevious == '\\' || fLiteral ) {
         *pchBuffer ++ = *pchPattern;
         continue;
      }
      if ( chCurrent == '#' || chCurrent == '0' || chCurrent == '.' )
         break;
      if ( chCurrent != ',' ) {
         *pchBuffer ++ = *pchPattern;
      }
   }
   char* pchBufferStart = pchBuffer;
   char* pchPatternStart = pchPattern;
   if ( fMantissa ) {
      StrReverse( pchDigits );
      StrReverse( pchPattern );
   }
   fLiteral = false;
   chPrevious = '\0';
   for ( ; *pchPattern; chPrevious = *pchPattern, pchPattern ++ ) {
      char chCurrent = *pchPattern;
      if ( chCurrent == '\"' ) {
         fLiteral = ( fLiteral ) ? false : true;
         continue;
      }
      if ( chCurrent == '\\' )
         continue;
      if ( chPrevious == '\\' || fLiteral ) {
         *pchBuffer ++ = *pchPattern;
         continue;
      }
      if ( chCurrent == '#' || chCurrent == '0' ) {
         if ( *pchDigits == '-' || *pchDigits == ',' )
            *pchBuffer ++ = *pchDigits ++;
         if ( *pchDigits )
            *pchBuffer ++ = *pchDigits ++;
      }
      else if ( chCurrent != ',' ) {
         *pchBuffer ++ = *pchPattern;
      }
   }
   while ( *pchDigits )
      *pchBuffer ++ = *pchDigits ++;
   *pchBuffer = '\0';
   if ( fMantissa ) {
      StrReverse( pchDigits );
      StrReverse( pchPatternStart );
      StrReverse( pchBufferStart );
   }
   return pchBuffer;
}
/*--------------------------------------------------------------------------------------------------
   tFormatter::Output - output the string representation of a double using the excel-like format
                        pattern. Returns an empty string on error.
                        NB: there is a 1024 character limit. longer output strings will be truncated.
  --------------------------------------------------------------------------------------------------*/
const char* tFormatter::Output( tFormatter::tSection* pSection,
                                char* pchMantissa, char* pchDecimal, char* pchExponent ) const
{
   char szMantissa[ 1024 ];
   strcpy( szMantissa, pchMantissa );
   pchMantissa = szMantissa;
   if ( pSection->fThousandsSep )
      InsertThousandsSep( pchMantissa );
   unsigned nchDecimal = strlen( pchDecimal );
   if ( nchDecimal > pSection->nMandatoryDecDigits ) {
      char* pchDec = &pchDecimal[ nchDecimal - 1 ];
      for ( unsigned uchDecimal = nchDecimal; uchDecimal > pSection->nMandatoryDecDigits; uchDecimal -- ) {
         if ( *pchDec == '0' )
            *pchDec -- = '\0';
         else
            break;
      }
   }
   char szPattern[ sizeof( m_szBuffer ) ];
   strncpy( szPattern, pSection->pchSection, sizeof( szPattern ) );
   if ( pSection->pchDecimal )
      szPattern[ pSection->pchDecimal - pSection->pchSection ] = '\0';
   else if ( pSection->pchExponent )
      szPattern[ pSection->pchExponent - pSection->pchSection ] = '\0';
   char* pchNext = OutputPattern( (char*) &m_szBuffer[ 0 ],
                                  ( m_nSections > 1 && *pchMantissa == '-' ) ? (pchMantissa + 1) : pchMantissa,
                                  szPattern, true );
   if ( pSection->pchDecimal ) {
      *pchNext ++ = '.';
      strncpy( szPattern, pSection->pchDecimal, sizeof( szPattern ) );
      if ( pSection->pchExponent )
         szPattern[ pSection->pchExponent - pSection->pchDecimal ] = '\0';
      pchNext = OutputPattern( pchNext, pchDecimal, &szPattern[ 1 ], false );
   }
   if ( pSection->pchExponent ) {
      *pchNext ++ = pSection->pchExponent[ 0 ];
      if ( pSection->pchExponent[ 1 ] != '-' || pchExponent[ 0 ] != '+' )
         *pchNext ++ = *pchExponent ++;
      else
         pchExponent ++;
      pchNext = OutputPattern( pchNext, pchExponent, &pSection->pchExponent[ 2 ], false );
   }
   *pchNext = '\0';
   return m_szBuffer;
}
/*--------------------------------------------------------------------------------------------------
   tFormatter::Output - output the string representation of a double using the excel-like format
                        pattern. Returns an empty string on error.
                        NB: there is a 1024 character limit. longer output strings will be truncated.
  --------------------------------------------------------------------------------------------------*/
const char* tFormatter::Output( double dValue ) const
{
   // get the section relevant to the input number.
   tFormatter::tSection* pSection = GetSection( dValue );
   if ( pSection == NULL )
      return NULL;
   // no numerics in pattern - just output the pattern.
   if ( pSection->nMantissaDigits == 0U && pSection->nOptionalDecDigits == 0U && pSection->nExponentDigits == 0U )
   {
      strncpy( (char*) &m_szBuffer[0], pSection->pchSection, sizeof( m_szBuffer ) );
      ((char*) m_szBuffer)[ sizeof( m_szBuffer )-1 ] = '\0';
      return &m_szBuffer[0];
   }
   if ( dValue != 0.0 )
      dValue *= pSection->dScale;
   char szValue[ 1024 ];
   char szExpon[ 64 ];
   char* pchMantissa;
   char* pchDecimal;
   char* pchExponent;
   if ( pSection->pchExponent ) {
      double dExponent;
      double dSignif;
      if ( dValue == 0.0 ) {
         dExponent = 0.0;
         dSignif = 0.0;
      }
      else {
         dExponent = ceil( log10( fabs( dValue ) ) ) - 1.0;
         dSignif = dValue / pow( 10.0, dExponent );
      }
      dExponent -= (double) ( (( int ) pSection->nMantissaDigits) - 1 );
      dSignif *= pow( 10.0, (( int ) pSection->nMantissaDigits) - 1 );
      sprintf( szValue,
               "%*.*lf",
               ( int )    pSection->nMantissaDigits + pSection->nOptionalDecDigits + 1U,
               ( int )    pSection->nOptionalDecDigits,
               ( double ) dSignif );
      if ( pSection->pchExponent[ 1 ] != '-' && dExponent >= 0.0 ) {
         sprintf( szExpon,
                  "%c+%0*d",
                  ( char ) pSection->pchExponent[ 0 ],
                  ( int ) pSection->nExponentDigits,
                  ( int ) dExponent );
      }
      else {
         sprintf( szExpon,
                  "%c%0*d",
                  ( char ) pSection->pchExponent[ 0 ],
                  ( int ) ( dExponent < 0.0 ) ? (pSection->nExponentDigits + 1U) : pSection->nExponentDigits,
                  ( int ) dExponent );
      }
      pchExponent = &szExpon[ 1 ];
   }
   else {
      if ( dValue == 0.0 ) {
         sprintf( szValue,
                  "%0*.*lf",
                  ( int )    pSection->nMantissaDigits + pSection->nOptionalDecDigits + 1U,
                  ( int )    pSection->nOptionalDecDigits,
                  ( double ) dValue );
      }
      else {
         unsigned nTotWidth = pSection->nMantissaDigits + pSection->nOptionalDecDigits + 1U;
         if ( dValue < 0.0 )
            nTotWidth ++;
         if ( pSection->nOptionalDecDigits == 0U )
            nTotWidth --;
         sprintf( szValue, "%0*.*lf", ( int ) nTotWidth, ( int ) pSection->nOptionalDecDigits, ( double ) dValue );
      }
   }
   if ( pSection->nMantissaDigits == 0U ) {
      if ( strncmp( szValue, "0.", 2 ) == 0 )
         memmove( szValue, &szValue[1], strlen( szValue ) );
      else if ( strncmp( szValue, "-0.", 3 ) == 0 )
         memmove( &szValue[1], &szValue[2], strlen( szValue )-1 );
   }
   pchMantissa = szValue;
   pchDecimal = strchr( szValue, '.' );
   if ( pchDecimal )
      *pchDecimal ++ = '\0';
   else
      pchDecimal = &pchMantissa[ strlen( pchMantissa ) ];
   return Output( pSection, pchMantissa, pchDecimal, pchExponent );
}
/*--------------------------------------------------------------------------------------------------
   tFormatter::Input - read the formatted double represented by the excel-like format pattern.
                       Returns HUGE_VAL on error.
  --------------------------------------------------------------------------------------------------*/
double tFormatter::Input( const char* pszText ) const
{
   bool fPeriod = false;
   bool fExponent = false;
   bool fDigit = false;
   char szBuffer[ 1024 ];
   char* pchBuffer = &szBuffer[0];
   for ( char* pchText = (char*) pszText; *pchText; pchText ++ )
   {
      if ( *pchText == '.' ) {
         if ( ! fPeriod ) {
            fPeriod = true;
            *pchBuffer ++ = '.';
         }
         continue;
      }
      if ( toupper( *pchText ) == 'E' && ( pchText[ 1 ] == '+' || isdigit( pchText[ 1 ] ) ) ) {
         if ( ! fExponent ) {
            fExponent = true;
            *pchBuffer ++ = *pchText ++;
            *pchBuffer ++ = *pchText;
         }
         continue;
      }
      if ( isdigit( *pchText ) ) {
         fDigit = true;
         *pchBuffer ++ = *pchText;
         continue;
      }
      if ( ! fDigit && ( *pchText == '+' || *pchText == '-' ) ) {
         *pchBuffer ++ = *pchText;
         continue;
      }
   }
   *pchBuffer = '\0';
   double dValue = atof( szBuffer );
   if ( fabs( dValue ) == HUGE_VAL )
      return HUGE_VAL;
   tFormatter::tSection* pSection = GetSection( dValue );
   dValue /= pSection->dScale;
   return dValue;
}

