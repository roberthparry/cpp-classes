/*

   29-05-95    RJP     -914 printing as -,914.00 
   
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "tformat.h"

/*----------------------------------------------------------------------------
   #defines
----------------------------------------------------------------------------*/
#define CH_PERIOD    '.'
#define CH_COMMA     ','
#define CH_SEMICOLON ';'
#define CH_POUND     '#'
#define CH_ZERO      '0'
#define CH_SPACE     ' '
#define MAXPAT    (int) 100

#ifndef DNULLVAL
   #define DNULLVAL     -1.0E300          // What represents a NULL
#endif

/*----------------------------------------------------------------------------
   tFormat constructor
----------------------------------------------------------------------------*/
tFormat::tFormat( )
:  m_nFieldSize( 0  )
,  m_dNull     (  DNULLVAL  )
{
   m_pszNull = strdup( "(null)" );
}

/*----------------------------------------------------------------------------
   tFormat destructor
----------------------------------------------------------------------------*/
tFormat::~tFormat( )
{
   free( m_pszNull  );
}

/*----------------------------------------------------------------------------
   tFormat::SetFormat
----------------------------------------------------------------------------*/
void tFormat::SetFormat (  const char* szPattern, int nFieldSize  )

{
   strncpy( m_szPattern, szPattern, sizeof(m_szPattern) );
   m_szPattern[ sizeof(m_szPattern)-1 ] = '\0';
   m_nFieldSize = nFieldSize;
}

/*----------------------------------------------------------------------------
   tFormat::SetWidth
----------------------------------------------------------------------------*/
void tFormat::SetWidth (  int nWidth  )
{
   m_nFieldSize = (nWidth > 60) ? 60 : nWidth;
}

/*----------------------------------------------------------------------------
   tFormat::SetNullDisplay
----------------------------------------------------------------------------*/
void tFormat::SetNullDisplay( const char* pszText  )
{
   free( m_pszNull  );
   m_pszNull = strdup( pszText  );
}

/*----------------------------------------------------------------------------
   tFormat::Output
----------------------------------------------------------------------------*/
const char* tFormat::Output (  double* dNumValue  )
{
   double dNum = *dNumValue;

   int nPrec = 0, nDec = 0 ;

   char szText[ 512 ];

   if (  dNum == m_dNull  ) {
      if (  m_pszNull  )
         sprintf( szText, "%*s", m_nFieldSize, m_pszNull  );
      else
         sprintf( szText, "%*s", m_nFieldSize, "(null)" );

      strncpy( m_szText, szText, sizeof( m_szText  ) );
      m_szText[ sizeof( m_szText  )-1 ] = '\0';
      return m_szText;
   }

   if (  m_nFieldSize > sizeof(m_szText) )
      return "*ERR8*";

   try
   {
      // create a non-const copy of szPattern
      char szWork[MAXPAT + 1] ;
      strcpy( szWork, m_szPattern  );

      char *apszPats[] = { NULL, NULL, NULL} ;

      // break out the masks
      char *pszMask = apszPats[0] = strtok( szWork, ";" );
      for (  int i = 1; pszMask != NULL && i < 3; i++ )
         pszMask = apszPats[i] = strtok( NULL, ";" );

      // default mask to 1st pattern
      pszMask = apszPats[0] ;

      char* pszSuffix = "";

      if (  dNum < 0.0 && apszPats[1] != NULL  ) {
         pszMask = apszPats[1] ; // select negative mask
         int nLast = pszMask ? strlen (pszMask) : 0;
         if ( (  nLast && pszMask[nLast-1] == '-' ) ||  (  nLast && pszMask[nLast-1] == ')' ) ) {
            dNum = -dNum ;
            pszSuffix = "-";
         }
      }
      else
         if (  dNum == 0.0 && apszPats[2] != NULL  )
         pszMask = apszPats[2] ; // select positive mask

      if (  pszMask == NULL  ) {
         return "*ERR1*";
      }


      // find precision for sprintf( )
      const char* pszPat = strchr (  pszMask, CH_PERIOD  );
      const char* pszPeriod = pszPat ? pszPat - 1 : pszMask;

      if (  pszPat  ) {
         while ( *++pszPat  )
            if ( *pszPat == CH_POUND || *pszPat == CH_ZERO  )
               nPrec++ ;
      }

      // mark the first placeholder in the pattern
      int nMask = strcspn( pszMask, "#0" );

      char szTextWork[ 1024 ];

      memset (  szTextWork, 0, sizeof(szTextWork) );

      int nBytes;

      nBytes = sprintf( szTextWork, "%-*.*lf%s", m_nFieldSize, nPrec, dNum, pszSuffix  ) + 1;

      if (  nBytes < m_nFieldSize  ) {
         nBytes = m_nFieldSize;
      }
      else
         if (  nBytes > sizeof(m_szText) ) {
         nBytes = sizeof(m_szText) - 2;
      }

      // the constructed number will be shifted in on the extreme right
      // of the destination field (  from right to left  ).
      char* pszDest = m_szText + nBytes;

      pszDest [1] = 0;

      // find the end of the string sprintf( ) created
      char *pszSrc = strchr (  szTextWork, CH_SPACE  );

      // if trailing spaces, back up to last digit or end of field
      pszSrc = pszSrc ? pszSrc - 1 : szTextWork + strlen (  szTextWork  ) - 1 ;

      bool bInFrac = nPrec > 0 ;
      bool bHasCommas = false ;
      bool bSig = false ;

      // point to the right side of the pattern
      pszPat = pszMask + strlen (  pszMask  );

      // shift in format characters
      while ( (  pszSrc >= szTextWork  ) && (  pszDest >= m_szText  ) )
      {
         if (  pszPat < pszMask  )
         {
            *pszDest-- = *pszSrc-- ;
            continue;
         }

         switch ( *pszPat  )
         {   
            case CH_ZERO:
               *pszDest-- = *pszSrc-- ;
               bSig = true ;
               break ;

            case CH_POUND:
               if (  bInFrac  ) {
                  if (  bSig || *pszSrc != CH_ZERO  ) {
                     *pszDest-- = *pszSrc-- ;
                     bSig = true ;
                  }
                  else
                     --pszSrc ;
               }
               else
                  if (  bSig && dNum > 0.0 && dNum < 1.0  )
                  --pszSrc ;
               else
                  *pszDest-- = *pszSrc-- ;
               break ;

            case CH_PERIOD:
               if (  bSig  )
                  *pszDest-- = *pszSrc ;
               --pszSrc ;
               bInFrac = false ;   // signal end of fractional part
               break ;

            default:
               if ( *pszPat == CH_COMMA  ) {
                  bHasCommas = true ;
                  if (  pszSrc == szTextWork && (  szTextWork[0] == '-' ) ) {
                     break;
                  }
               }
               *pszDest-- = *pszPat ;  // shift in pattern literals
               break ;
         }

         --pszPat ;

         // we need to extend the pattern until we exhaust the source
         if (  pszPat < pszMask + nMask && pszSrc >= szTextWork  )
            pszPat = bHasCommas ? pszMask + nMask + 3 : pszMask + nMask ;

      }

      // shift in any remaining mask literals
      for ( ; pszPat >= pszMask && pszDest >= m_szText; --pszPat  )
         if ( *pszPat != CH_POUND && *pszPat != CH_COMMA  )
            *pszDest-- = *pszPat ;

         // blank unused destinition
      for ( ; pszDest >= m_szText; --pszDest  )
         *pszDest = CH_SPACE ;


   }
   catch ( ... )
   {
      return "*ERR9*";
   }

   return m_szText;
}

/*----------------------------------------------------------------------------
   tFormat::Output
----------------------------------------------------------------------------*/
const char* tFormat::Output (  double dNum  )
{
   return Output ( &dNum  );
}

/*----------------------------------------------------------------------------
   tFormat::OutputNoLeadingSpaces
----------------------------------------------------------------------------*/
const char* tFormat::OutputNoLeadingSpaces (  double dNum  )
{
   return OutputNoLeadingSpaces ( &dNum  );
}

/*----------------------------------------------------------------------------
   tFormat::OutputNoLeadingSpaces
----------------------------------------------------------------------------*/
const char* tFormat::OutputNoLeadingSpaces (  double* pdNum  )
{
   char* pOutput = (char* ) Output (  pdNum  );
   char* pSkip;

   for (  pSkip = pOutput; ( *pSkip && ( *pSkip==' ' ) ); pSkip++ );
   
   return pSkip;
}

/*----------------------------------------------------------------------------
   tFormat::Output
----------------------------------------------------------------------------*/
const char* tFormat::Output (  int iValue  )
{
   double dNum = (double) iValue;
   return Output ( &dNum  );
}

/*----------------------------------------------------------------------------
   tFormat::OutputNoLeadingSpaces
----------------------------------------------------------------------------*/
const char* tFormat::OutputNoLeadingSpaces (  int iValue  )
{
   double dNum = (double) iValue;
   return OutputNoLeadingSpaces ( &dNum  );
}


/*----------------------------------------------------------------------------
   Number
----------------------------------------------------------------------------*/
bool Number (  char* pszNumber, double& dValue  )
{
   if ( ! pszNumber  ) return false;

   char* pszText = new char [ strlen (  pszNumber  ) + 2 ];

   if ( ! pszText  ) return false;

   pszText [0] = ' ';

   for (  char* pszDest = pszText + 1; *pszNumber; pszNumber++ ) {
      if (  isdigit ( *pszNumber  ) || '.' == *pszNumber || '-' == *pszNumber  ) {
         *pszDest++ = *pszNumber;
      }
      else {
         if ( '(' == *pszNumber  ) {
            pszText [0] = '-';
         }
      }
   }

   *pszDest = 0;

   bool bResult = sscanf (  pszText, "%lf", &dValue  ) > 0 ? true : false;

   delete [] pszText;

   return bResult;
}

/*----------------------------------------------------------------------------
   Number
----------------------------------------------------------------------------*/
bool tFormat::Number (  char* pszNumber, double& dValue, double dDef, bool bUseDef  )
{
   bool bResult = false;

   try {
      bResult = ::Number (  pszNumber, dValue  );
   }
   catch ( ... ) {
   }

   if ( ! bResult  ) {
      dValue = dDef;
   }

   return bResult;
}

/*----------------------------------------------------------------------------
   Number
----------------------------------------------------------------------------*/
bool Number (  char* pszNumber, int& iValue  )
{
   if ( ! pszNumber  ) return false;

   char* pszText = new char [ strlen (  pszNumber  ) + 2 ];

   if ( ! pszText  ) return false;

   pszText [0] = ' ';

   for (  char* pszDest = pszText + 1; *pszNumber; pszNumber++ ) {
      if (  isdigit ( *pszNumber  ) || '.' == *pszNumber || '-' == *pszNumber  ) {
         *pszDest++ = *pszNumber;
      }
      else {
         if ( '(' == *pszNumber  ) {
            pszText [0] = '-';
         }
      }
   }

   *pszDest = 0;

   bool bResult = sscanf (  pszText, "%d", &iValue  ) > 0 ? true : false;

   delete [] pszText;

   return bResult;
}

/*----------------------------------------------------------------------------
   Number
----------------------------------------------------------------------------*/
bool tFormat::Number ( char* pszNumber, int& iValue, int iDef, bool bUseDef )
{
   bool bResult = false;

   try
   {
      bResult = ::Number ( pszNumber, iValue );
   }
   catch ( ... )
   {
   }

   if ( ! bResult  )
      iValue = iDef;

   return bResult;    
}
