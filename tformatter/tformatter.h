/*
 * Name:           tformatter.h
 * Copyright:      BB&D
 * Author:         R. Parry
 * Date:           22/12/03
 *
 * Description:    For Excel-style double formatting.
 *
 */

#ifndef  _tFormatter_h_
#define  _tFormatter_h_

/*--------------------------------------------------------------------------------------------------
   tFormatter class - use this class to format doubles using excel-style format patterns.
  --------------------------------------------------------------------------------------------------*/
class tFormatter {
private:

   // Section indicator
   enum   ESection {
      NegativeSection         = 0,
      PositiveSection,     // = 1,
      ZeroSection,         // = 2
   };
   
   // Description of the format pattern for a single section.
   struct tSection {
      unsigned             nMantissaDigits;     // number of mandatory digits in the mantissa.
      unsigned             nOptionalDecDigits;  // number of optional digits in the decimal.
      unsigned             nMandatoryDecDigits; // number of mandatory digits in the decimal.
      unsigned             nExponentDigits;     // number of mandatory exponent digits.
      char*                pchSection;          // place holder for section.
      char*                pchDecimal;          // place holder for the decimal point.
      char*                pchExponent;         // place holder for the exponent.
      double               dScale;              // scale multiplication factor.
      bool                 fPercent;            // is a percentage.
      bool                 fThousandsSep;       // has a thousands separator.
   };
   
   unsigned                m_nSections;         // number of sections.
   tFormatter::tSection*   m_pSections;         // pointer to sections.
   char*                   m_pszPattern;        // pattern.

   /* mutable */
   char                    m_szBuffer[ 1024 ];  // output buffer.
   
private:

   tFormatter::tSection*   GetSection           ( double dValue ) const;
   tFormatter::tSection*   GetSection           ( ESection eSection ) const;
   bool                    CompleteSection      ( tFormatter::tSection* pSection );
   void                    LocalCopy            ( const tFormatter& fmt );
   void                    LocalDelete          (); 
   char*                   OutputPattern        ( char* pchBuffer, char* pchDigits, char* pchPattern,
                                                  bool fMantissa ) const;
   const char*             Output               ( tFormatter::tSection* pSection,
                                                  char* pchMantissa, char* pchDecimal, char* pchExponent ) const;
public:

                           tFormatter           ( const char* pszPattern = 0 );
                          ~tFormatter           ();
  
                           tFormatter           ( const tFormatter& fmt );
   tFormatter&             operator=            ( const tFormatter& fmt );

   bool                    SetPattern           ( const char* pszPattern );
  
   const char*             Output               ( double dValue ) const;
   double                  Input                ( const char* pszText ) const;
   
};

#endif //tFormatter_h_

