/*
 *
 * tDate.h
 *
 */


#ifndef _tDate_h_
#define _tDate_h_

#ifndef __cplusplus
#error  Requires C++ Compiler
#endif

#ifndef  _INC_TIME
   #include <time.h>
#endif

#ifndef  _tLiteral_h_
   #include "tliteral.h"
#endif

///////////////////////////////////////////////////////////////////////////////////////////
enum EPeriod
{
   eDay        = 1,
   eWeek       = 2,
   eMonth      = 3,
   eQuarter    = 4,
   eSemester   = 5,
   eYear       = 6
};

///////////////////////////////////////////////////////////////////////////////////////////
// tDate class declaration
//
class tDate {

public:
   const enum { OFF, ON };
   const enum { BUF_SIZE = 128 };
   const enum {NO_CENTURY  = ( unsigned char ) 0x02,
               DATE_ABBR   = ( unsigned char ) 0x04,
               INC_TIME    = ( unsigned char ) 0x80};
   const enum  EWeekDay { SUNDAY = 1, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY };

//protected:
private:
            long           m_lJulian;                 // See JulianDate()  Days since 1/1/4713 B.C.
            int     			m_iYear;                   // See Year()        eg 1994
            unsigned char  m_cMonth;                  // See Month()
            unsigned char  m_cDay;                    // See Day()
            unsigned char  m_eDayOfWeek;              // See DayOfWeek()   1 = Sunday, ... 7 = Saturday
            unsigned char  m_cHour;
            unsigned char  m_cMinute;
            unsigned char  m_cSecond;

private:
   static   unsigned int	g_nStartDST;
   static   unsigned int 	g_nStartSTD;
   static   int         	g_riFirstDayOfMonth [ 12 ];
   static   int         	g_riLastDayOfMonth  [ 12 ];
   static   EWeekDay    	g_iLastDayOfWeek;

            void        JulianToMDY();             // Convert julian day to mdy
   inline   void        JulianToDOW();             // Convert julian day to day_of_week
            void        MDYToJulian();             // Convert mdy to julian day

   static   int         DayDiff             ( int iDay1, int iDay0 );

public:
                        
                        tDate ( const char* YYYYMMDDHH24MISS );// YYYYMMDD is OK too - 235959 will be assumed.
                        tDate ( const tDate& dt );

                        tDate ( void );
                        tDate ( long j );
                        tDate ( int iMonth, int iDay, int iYear, int iHour = 0, int iMin = 0, int iSec = 0 );
                        tDate ( const tm& tmdate );
                        tDate ( int iDayInYear, int iYear );
                        tDate ( double ExcelDate );            // To be used with serial dates in Excel/VBA
                                                               // For definition of structure see source
   virtual             ~tDate ();

   // Validation
            bool        IsValid ( void ) const;
            void        Clear   ( void );

   // Format a date according to certain rules:
   // %d   : day number - minimum digits.
   // %dd  : day number - always 2 digits.
   // %ddd : shortened day of week ( eg mon, tue, wed, ... )
   // %Ddd : Shortened day of week ( eg Mon, Tue, Wed, ... )
   // %DDD : SHORTENED day of week ( eg MON, TUE, WED, ... )
   // %dddd: full day of week ( eg monday, tuesday, ... )
   // %Dddd: Full day of week ( eg Monday, Tuesday, ... )
   // %DDDD: FULL day of week ( eg MONDAY, TUESDAY, ... )
   // %o   : cardinal for day number ( eg st, nd, th, ... )
   // %O   : cardinal for day number ( eg ST, ND, TH, ... )
   // %m   : month number - minimum digits.
   // %mm  : month number - always 2 digits.
   // %mmm : shortened month name ( eg jan, feb, ... )
   // %Mmm : Shortened month name ( eg Jan, Feb, ... )
   // %MMM : SHORTENED month name ( eg JAN, FEB, ... )
   // %mmmm: full month name ( eg january, february, ... )
   // %Mmmm: Full month name ( eg January, February, ... )
   // %MMMM: FULL month name ( eg JANUARY, FEBRUARY, ... )
   // %yy  : short year ( eg 97 )
   // %yyyy: full year ( eg 1997 )
   // @h   : hour - minimum digits.
   // @hh  : hour - always 2 digits.
   // @Hh  : hour in 24 hour clock - always 2 digits.
   // @m   : minute - minimum digits.
   // @mm  : minute - always 2 digits.
   // @s   : second - minimum digits.
   // @ss  : second - always 2 digits.
   // @p   : pm / am.
   // @P   : PM / AM.
   // %%   : '%' char.
   // @@   : '@' char.
   // ^    : void character ( eg "@h^hr" -> "9hr" and "@^@h" -> "@9" )
   tLiteral&            Format   ( tLiteral& strBuffer, const char* lpszFormat ) const;

   // JVdE, 30/06/1996: added these two
   //
   // Return next/previous day that falls on given weekday
   // If this day is already that weekday return one week
   // later/earlier
   tDate                Next     ( EWeekDay eWeekDay );
   tDate                Previous ( EWeekDay eWeekDay );
                        
   const    tDate&      operator = ( const tDate& dt );
                        
   inline   tDate       operator + ( long i ) const;
   inline   tDate       operator + ( int  i ) const;
                        
   inline   tDate       operator - ( long i ) const;
   inline   tDate       operator - ( int  i ) const;
   inline   long        operator - ( const tDate& dt ) const;

   inline const tDate&  operator += ( long i );
   inline const tDate&  operator -= ( long i );

   inline   tDate       operator ++ ();            // Prefix increment
            tDate       operator ++ ( int );       // Postfix increment
   inline   tDate       operator -- ();            // Prefix decrement
            tDate       operator -- ( int );       // Postfix decrement

   inline   int         operator <  ( const tDate& dt ) const;
   inline   int         operator <= ( const tDate& dt ) const;
   inline   int         operator >  ( const tDate& dt ) const;
   inline   int         operator >= ( const tDate& dt ) const;
   inline   int         operator == ( const tDate& dt ) const;
   inline   int         operator != ( const tDate& dt ) const;

                        operator double ( void );   // tDate to Excel/VBA date, for structure see source
                                                    // of constructor taking double


// static conversions added 30/04/1996 by JVdE
   static   bool        JulianToMDY ( long lJulian, int& iMonth, int& iDay, int& iYear );
   static   bool        MDYToJulian ( int iMonth, int iDay, int iYear, long& lJulian );


   static   unsigned short	YearInFull    ( unsigned short usYear );
   static   int         	GetDaysInYear ( int iYear );           // Returns number of days in year
   static   bool         	IsLeapYear    ( int iYear );           // returns true if leap year, false if not

   static   bool        	SetLastDayOfWeek  ( EWeekDay iWeekDay ); // true if iWeekDay valid
   static   EWeekDay    	GetLastDayOfWeek  ( void );              // Returns ( SUNDAY .. SATURDAY )
   static   EWeekDay    	GetFirstDayOfWeek ( void );              // Returns ( SUNDAY .. SATURDAY )

   inline   long        	JulianDate () const;        // Returns julian date
   inline   int         	DayOfYear  () const;        // Returns relative date since Jan. 1
            bool        	IsLeapYear () const;        // Returns true if leap year, false if not
            bool        	IsDST      () const;        // Returns true if date is within Daylight Savings Time ( DST ), false if not

         /*
          * Sets the month and day which DST and STD date begins!  This will
          * enable IsDST() to return the correct result for regions other than
          * North America.  Returns true if month and day values are valid,
          * false otherwise 
          */
            bool        SetDST( int iMonth, int iDay ) const;
            bool        SetSTD( int iMonth, int iDay ) const;

         // The next functions return a tm structure

            tm eom() const;               // Returns last day of month in object
            tm GetDate() const;           // Returns a tm structure

         // Various static conversions
   static   bool        DateToSystemTime  ( const tDate& date, tm& tmdate );
   static   bool        SystemTimeToDate  ( const tm& tmdate, tDate& date );
   static   bool        DateToExcelDate   ( const tDate& date, double& ExcelDate );
   static   bool        ExcelDateToDate   ( const double& ExcelDate, tDate& date );
   static   bool        DateToEruDate     ( const tDate& date, int& iEruDate, int iFreq );

   // Convert an tDate to an ERU date using AdjustedDateToEruDate modifies
   // the tDate to point to the beginning/end of the period the data falls
   // into taking the given frequency into consideration
   static   bool        AdjustedDateToEruDate ( tDate& date, int& iEruDate, int iFreq, bool fStartOfPeriod = false );
   static   bool        EruDateToDate         ( int iEruDate, tDate& date, int iFreq, bool fStartOfPeriod );

   static   bool        IsValidDate           ( int iMonth, int iDay, int iYear );

        /*
         * These 'Set's modify the date object and actually SET it
         * They all return a reference to self ( *this )
         */

   const    tDate&      SetToday  ( void );         // Sets to current system date
   inline const tDate&  Set       ( long lJulian );
   const    tDate&      Set       ( int iMonth, int iDay, int iYear, int hr = 0, int min = 0, int sec = 0 );
   const    tDate&      Set       ( int iDayInYear, int iYear );
   const    tDate&      SetYMD    ( int iYMDDate );

   // Return today's date ( added by JVdE on 30/04/96 )
   static   tDate       Today     ( void );

	static   tDate       EasterSunday( int iYear );

         // May also pass neg# to decrement
   inline const tDate&   AddWeeks  ( int iCount = 1 );
          const tDate&   AddMonths ( int iCount = 1 );
   inline const tDate&   AddYears  ( int iCount = 1 );

   inline   int         GetYMD    ( void ) const { return ( GetSortableDate () ); }

   inline   int         Day() const;               // Numeric day of date object
            int         DaysInMonth() const;       // Number of days in month ( 1..31 )

   // Static version added 30/04/1996 by JVdE
   static   int         DaysInMonth ( int iMonth, int iYear );

   inline   int         FirstDayOfMonth() const;   // First day of month  ( 1..7 )

   inline   EWeekDay    DayOfWeek() const;         // ( SUNDAY .. SATURDAY )

   inline   int         WeekOfMonth() const;       // Numeric week of month  ( 1..6 )
            int         WeekOfYear() const;        // Numeric week of year   ( 1..53 )

            bool        IsInWeek53 ( void ) const;
            int         WeeksInYear() const;

   inline   int         Semester() const;             // Semester ( 1..2 )
   inline   tDate       BeginningOfSemester() const;  // First date of semester
   inline   tDate       EndOfSemester() const;        // Last date of semester

   inline   int         Quarter() const;             // Quarter ( 1..4 )
   inline   tDate       BeginningOfQuarter() const;  // First date of quarter
   inline   tDate       EndOfQuarter() const;        // Last date of quarter

   inline   int         Month() const;             // Month number ( 1..12 )
   inline   tDate       BeginningOfMonth() const;  // First date of month
   inline   tDate       EndOfMonth() const;        // Last date of month

   inline   int         Year() const;              // eg. 1994
   inline   tDate       BeginningOfYear() const;   // First date of year
   inline   tDate       EndOfYear() const;         // Last date of year


                                                   // First date of week
   inline   tDate        BeginningOfWeek ( EWeekDay eLastDayOfWeek = g_iLastDayOfWeek ) const;
                                                   // Last date of week
            tDate        EndOfWeek ( EWeekDay eLastDayOfWeek = g_iLastDayOfWeek ) const;

   // Time manipulation
   inline   int         Hour  () const;            // ( 0 .. 23 )
   inline   int         Minute() const;            // ( 0 .. 59 )
   inline   int         Second() const;            // ( 0 .. 59 )

            void        AddSeconds ( int iSeconds );
            void        AddMinutes ( int iMinutes );
            void        AddHours   ( int iHours   );
   
            int         GetSortableDate ( void ) const;  // Int of the form YYYYMMDD

   static   int         GetCurrentCentury ( void );      // 1900 or 2000 or whatever

   static   tDate*  		GetVectorDate        ( int iDate, int iFreq );
   static   tDate*  		GetNextVectorDate    ( tDate * pDate, int iFreq );
   static   void    		AddPeriodToDate      ( tDate& date, EPeriod period, int iCount = 1 );
   static   EPeriod		GetFrequencyAsPeriod ( int iFreq );
   static   int     		GetNumberOfPeriods   ( const tDate& start, const tDate& end, EPeriod period );

   friend   unsigned int HashKey             ( const tDate& date ) { return ( unsigned int ) date.m_lJulian; }
};


////////////////////////////////////////////////////////////////////////////////////////////
// tDate class inlines and free functions


// tDate Arithmetic

inline tDate tDate::operator + ( long lDays ) const
{
   return tDate ( ( long ) ( m_lJulian + lDays ) );
}


inline tDate tDate::operator + ( int iDays ) const
{
   return tDate ( ( long ) ( m_lJulian + ( long ) iDays ) );
}


inline tDate tDate::operator - ( long lDays ) const
{
   return tDate ( ( long ) ( m_lJulian - lDays ) );
}


inline tDate tDate::operator - ( int iDays ) const
{
   return tDate ( ( long ) ( m_lJulian - ( long ) iDays ) );
}


inline long tDate::operator - ( const tDate& dt ) const
{
   return ( m_lJulian - dt.m_lJulian );
}


inline const tDate &tDate::operator += ( long iDays )
{
   m_lJulian += iDays;
   JulianToMDY();

   return *this;
}


inline const tDate &tDate::operator -= ( long iDays )
{
   m_lJulian -= iDays;
   JulianToMDY();

   return *this;
}


inline tDate tDate::operator ++ ()
{
   m_lJulian++;
   JulianToMDY();
   return *this;
}


inline tDate tDate::operator -- ()
{
   m_lJulian--;
   JulianToMDY();

   return *this;
}


// tDate comparison

inline int tDate::operator < ( const tDate& dt ) const
{
   return m_lJulian < dt.m_lJulian;
}


inline int tDate::operator <= ( const tDate& dt ) const
{
   return ( ( m_lJulian == dt.m_lJulian ) || ( m_lJulian < dt.m_lJulian ) );
}


inline int tDate::operator > ( const tDate& dt ) const
{
    return m_lJulian > dt.m_lJulian;
}


inline int tDate::operator >= ( const tDate& dt ) const
{
    return ( ( m_lJulian == dt.m_lJulian ) || ( m_lJulian > dt.m_lJulian ) );
}


inline int tDate::operator == ( const tDate& dt ) const
{
    return m_lJulian == dt.m_lJulian;
}


inline int tDate::operator != ( const tDate &dt ) const
{
    return m_lJulian != dt.m_lJulian;
}


// Conversion routines

inline void tDate::JulianToDOW( void )
{
   //m_eDayOfWeek = ( EWeekDay ) ( ( m_lJulian + 2 ) % 7 + 1 );
   m_eDayOfWeek = ( EWeekDay ) ( m_lJulian % 7 + 1 );
}


// Miscellaneous Routines

inline long tDate::JulianDate( void ) const
{
   return m_lJulian;
}


inline int tDate::DayOfYear ( void ) const
{
   tDate dtTemp ( 1, 1, m_iYear );

   return ( int ) ( m_lJulian - dtTemp.m_lJulian + 1 );
}


inline const tDate& tDate::Set ( long lJulian )
{
   m_lJulian = lJulian;

   JulianToMDY();

   return *this;
}


inline int tDate::FirstDayOfMonth() const
{
   return tDate ( m_cMonth, 1, m_iYear ).DayOfWeek();
}


inline int tDate::Day() const
{
   return m_cDay;
}


inline tDate::EWeekDay tDate::DayOfWeek() const
{
   return ( EWeekDay ) m_eDayOfWeek;
}


inline int tDate::Year() const
{
   return m_iYear;
}


inline int tDate::Month() const
{
   return m_cMonth;
}


inline const tDate& tDate::AddWeeks( int iCount )
{
   Set( m_lJulian + 7 * ( long ) iCount );

   return *this;
}


inline const tDate& tDate::AddYears( int iCount )
{
   m_iYear += iCount;

   MDYToJulian();

   return *this;
}


inline int tDate::WeekOfMonth() const
{
   // Abs day includes the days from previous month that fills up the beginning of the week.
   int nAbsDay = m_cDay + FirstDayOfMonth() - 1;
   return ( nAbsDay - ( int ) DayOfWeek() ) / 7 + 1;
}


inline int tDate::Semester ( void ) const
{
   return ( m_cMonth <= 6 ? 1 : 2 );
}


inline tDate tDate::BeginningOfSemester ( void ) const
{
   return tDate ( ( ( m_cMonth - 1 ) / 6 ) * 6 + 1, 1, m_iYear );
}


inline tDate tDate::EndOfSemester ( void ) const
{
   return ( tDate ( ( ( m_cMonth - 1 ) / 6 ) * 6 + 6, 1, m_iYear ).EndOfMonth () );
}


inline int tDate::Quarter ( void ) const
{
   return ( ( m_cMonth - 1 ) / 3 + 1 );
}


inline tDate tDate::BeginningOfQuarter ( void ) const
{
   return tDate ( ( ( m_cMonth - 1 ) / 3 ) * 3 + 1, 1, m_iYear );
}


inline tDate tDate::EndOfQuarter ( void ) const
{
   return ( tDate ( ( ( m_cMonth - 1 ) / 3 ) * 3 + 3, 1, m_iYear ).EndOfMonth () );
}


inline tDate tDate::BeginningOfMonth ( void ) const
{
   return tDate( m_cMonth, 1, m_iYear );
}


inline tDate tDate::EndOfMonth ( void ) const
{
   if ( m_cMonth == 12 )
      // Avoid calling constructor with a month > 12...
      return ( tDate ( 12, 31, m_iYear ) );
   else
      return ( tDate ( m_cMonth + 1, 1, m_iYear ) - 1L );
}


inline tDate tDate::BeginningOfYear ( void ) const
{
   return ( tDate ( 1, 1, m_iYear ) );
}


inline tDate tDate::EndOfYear ( void ) const
{
   return tDate( 12, 31, m_iYear );
}


inline tDate tDate::BeginningOfWeek ( EWeekDay eLastDayOfWeek ) const
{
   return ( tDate ( *this ).EndOfWeek ( eLastDayOfWeek ) - 6L );
}


inline int tDate::Hour ( void ) const
{
   return ( m_cHour );
}


inline int tDate::Minute ( void ) const
{
   return ( m_cMinute );
}


inline int tDate::Second ( void ) const
{
   return ( m_cSecond );
}


#endif //_tDate_h_

