/*
 *
 * tDate.cpp
 *
 */

#if defined( __BORLANDC__ )
   #include <ctype.h>
#else
   #include <minmax.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "tdate.h"

//////////////////////////////////////////////////////////////////////////////
static inline long Div( long l1, long l2 )
{
   return ((l1 >= 0L) ? (l1 / l2) : ((l1 - l2 + 1L) / l2));
}

//////////////////////////////////////////////////////////////////////////////
unsigned int  tDate::g_nStartDST  = ( ( 4 << 8 ) + 1 );
unsigned int  tDate::g_nStartSTD  = ( ( 10 << 8 ) + 31 );


int tDate::g_riFirstDayOfMonth [ 12 ] = 
{ 
   1,   // Jan
   32,  // Feb
   60,  // Mar
   91,  // Apr
   121, // May
   152, // Jun
   182, // Jul
   213, // Aug
   244, // Sep
   274, // Oct
   305, // Nov
   335  // Dec
};


int tDate::g_riLastDayOfMonth [ 12 ] = 
{ 
   31,  // Jan
   59,  // Feb
   90,  // Mar
   120, // Apr
   151, // May
   181, // Jun
   212, // Jul
   243, // Aug
   273, // Sep
   304, // Oct
   334, // Nov
   365  // Dec
};

//////////////////////////////////////////////////////////////////////////////
// JVdE, 22/9/95: Changed to FRIDAY
tDate::EWeekDay tDate::g_iLastDayOfWeek = FRIDAY;


//////////////////////////////////////////////////////////////////////////////
inline static long ComputeDayNumber( long lDay, long lMonth, long lYear )
{
   long lJulian;
   tDate::MDYToJulian( (int) lMonth, (int) lDay, (int) lYear, lJulian );
   return (long) lJulian;
}

//////////////////////////////////////////////////////////////////////////////
static long ComputeWeeksInYear( long lYear, long lWeekDayStart );

//////////////////////////////////////////////////////////////////////////////
// If the week belongs to the previous year, the value returned is a negative
// number of which the absolute value represents the final week number in the
// previous year. If the week belongs to the following year, the value returned
// is -1L.
static long ComputeWeekOfYear( long lDay, long lMonth, long lYear, long lStartOfWeek )
{
   long lDayNumberCommencing = ComputeDayNumber( 1L, 1L, lYear );
   long lDayNumber = ComputeDayNumber( lDay, lMonth, lYear );

   // If we are in the final 7 days of the year take special care in case the
   // date belongs to the following year.
   if ( lMonth == 12L && lDay > 24L ) {
      if ( ComputeWeekOfYear( lDay-24L, 1L, lYear+1L, lStartOfWeek ) == 2L )
         return -1L;
   }

   long lDayOfWeekCommencing = 1L + (lDayNumberCommencing % 7L);
   long lDaysFromFirstWeekBeginning = (7L + lDayOfWeekCommencing - lStartOfWeek) % 7L;
   
   long lWeekCountOrigin = lDayNumberCommencing - lDaysFromFirstWeekBeginning;
   if ( lDaysFromFirstWeekBeginning > 3L )
      lWeekCountOrigin += 7L;
   
   long lWeekCountOffset = lDayNumber - lWeekCountOrigin;
   long lWeekNumber = ( lWeekCountOffset < 0L )
                    ? -ComputeWeeksInYear( (lYear-1L), lStartOfWeek )
                    : ( lWeekCountOffset / 7L + 1L );

   return lWeekNumber;
}

//////////////////////////////////////////////////////////////////////////////
static long ComputeWeeksInYear( long lYear, long lWeekDayStart )
{
   long lDayNumber = ComputeDayNumber( 31L, 12L, lYear );
   long lDayOfWeek = (lDayNumber % 7L) + 1L;
   long lDay = ( ( (lDayOfWeek - lWeekDayStart + 7L) % 7L ) < 3L ) ? 24L : 31L;
   return ComputeWeekOfYear( lDay, 12L, lYear, lWeekDayStart );
}

//////////////////////////////////////////////////////////////////////////////
tDate::tDate ( const char* YYYYMMDDHH24MISS )
{
   char work[5];
   
   memcpy(work, YYYYMMDDHH24MISS, 4);    
   work[4] = 0;
   m_iYear = (int) atoi(work);
   
   memcpy(work, YYYYMMDDHH24MISS + 4, 2);
   work[2] = 0;
   m_cMonth = (unsigned char) atoi(work);

   memcpy(work, YYYYMMDDHH24MISS + 6, 2);
   work[2] = 0;
   m_cDay = (unsigned char) atoi(work);
   
   if ( YYYYMMDDHH24MISS[ 8 ] ) {
      memcpy(work, YYYYMMDDHH24MISS + 8, 2);
      work[2] = 0;
      m_cHour = (unsigned char) atoi(work);

      memcpy(work, YYYYMMDDHH24MISS + 10, 2);
      work[2] = 0;
      m_cMinute = (unsigned char) atoi(work);

      memcpy(work, YYYYMMDDHH24MISS + 12, 2);
      work[2] = 0;
      m_cSecond = (unsigned char) atoi(work);
   }
   else {
      m_cHour = 23;
      m_cMinute = 59;
      m_cSecond = 59;
   }
   
   m_eDayOfWeek = (EWeekDay) 0;

   MDYToJulian();
}

//////////////////////////////////////////////////////////////////////////////
tDate::tDate( const tDate& dt )
{
   m_cMonth  = dt.m_cMonth;
   m_cDay    = dt.m_cDay;
   m_iYear   = dt.m_iYear;
   m_cHour   = dt.m_cHour;
   m_cMinute = dt.m_cMinute;
   m_cSecond = dt.m_cSecond;

   MDYToJulian();
}

//////////////////////////////////////////////////////////////////////////////
tDate::tDate( void )
{
   m_cMonth = m_cDay = 0;
   m_eDayOfWeek = (EWeekDay) 0;
   m_iYear = 0;
   m_lJulian = 0L;
   m_cHour = m_cMinute = m_cSecond = 0;
}

//////////////////////////////////////////////////////////////////////////////
tDate::tDate( long lJulian ) : m_lJulian( lJulian )
{
   m_cMonth = m_cDay = 0;
   m_eDayOfWeek = (EWeekDay) 0;
   m_iYear = 0;
   m_cHour = m_cMinute = m_cSecond = 0;

   JulianToMDY();
}

//////////////////////////////////////////////////////////////////////////////
tDate::tDate( int iMonth,
              int iDay,
              int iYear, 
              int iHour   /* = 0 */, 
              int iMinute /* = 0 */, 
              int iSecond /* = 0 */ ) : 
   m_iYear        ( iYear ),
   m_cMonth       ( (unsigned char) iMonth ), 
   m_cDay         ( (unsigned char) iDay ), 
   m_cHour        ( (unsigned char) iHour ),
   m_cMinute      ( (unsigned char) iMinute ),
   m_cSecond      ( (unsigned char) iSecond )
{
   if ( iMonth > 12 ) {
      // Look at implementation of tDate::EndOfMonth( )!
      // might pass in a month > 12!
      m_cMonth -= (unsigned char) 12;
      m_iYear  ++;
   }

   MDYToJulian();
}


//////////////////////////////////////////////////////////////////////////////
tDate::tDate( const tm& tmdate )
{
   m_cMonth   = (unsigned char) tmdate.tm_mon;
   m_cDay     = (unsigned char) tmdate.tm_mday;
   m_iYear    = (int)  	 		  tmdate.tm_year;
   m_cHour    = (unsigned char) tmdate.tm_hour;
   m_cMinute  = (unsigned char) tmdate.tm_min;
   m_cSecond  = (unsigned char) tmdate.tm_sec;

   MDYToJulian();
}


//////////////////////////////////////////////////////////////////////////////
// Remark on structure of Excel dates
//
// Dates and times are stored internally as different parts of a real number.
// The value to the left of the decimal represents a date between January 1, 100 and December 31, 9999, 
// inclusive.  Negative values represent dates prior to December 30, 1899.
// The value to the right of the decimal represents a time between 0:00:00 and 23:59:59, inclusive.  
// Midday is represented by .5.
tDate::tDate( double ExcelDate )            // To be used with serial dates in Excel/VBA
{
	Clear();
   ExcelDateToDate( ExcelDate, *this );    // *this implicitly casted to tDate&
}

//////////////////////////////////////////////////////////////////////////////
tDate::~tDate( )
{
}

//////////////////////////////////////////////////////////////////////////////
void tDate::Clear( void )
{
   m_cMonth 	 = 0;
	m_cDay 		 = 0;
   m_eDayOfWeek = (EWeekDay) 0;
   m_iYear 		 = 0;
   m_lJulian 	 = 0L;
   m_cHour 		 = 0;
	m_cMinute 	 = 0;
	m_cSecond 	 = 0;
}

//////////////////////////////////////////////////////////////////////////////
const tDate& tDate::operator=( const tDate& dt )
{
   if ( &dt != this ) {
      m_cMonth       = dt.m_cMonth;
      m_cDay         = dt.m_cDay;
      m_iYear        = dt.m_iYear;
      m_cHour        = dt.m_cHour;
      m_cMinute      = dt.m_cMinute;
      m_cSecond      = dt.m_cSecond;

      MDYToJulian();
   }

   return *this;
}

//////////////////////////////////////////////////////////////////////////////
tDate::tDate( int iDayInYear, int iYear )
{
   m_iYear = iYear;

   bool fLeap = IsLeapYear ( );

   m_cHour   = 0;
   m_cMinute = 0;
   m_cSecond = 0;
   m_cMonth  = 0;
   m_cDay    = 0;

	for ( int iMonth = 0; m_cMonth == 0 && iMonth < 12; iMonth ++ ) {
		if ( fLeap ) {
			if ( iMonth == 0 ) {
				if ( iDayInYear >= g_riFirstDayOfMonth [ iMonth ] &&
					  iDayInYear <= g_riLastDayOfMonth  [ iMonth ] )
					m_cMonth = (unsigned char) (iMonth + 1);
				m_cDay = (unsigned char) (iDayInYear - g_riFirstDayOfMonth [ iMonth ] + 1);
			}
			else if ( iMonth == 1 ) {
				if ( iDayInYear >= g_riFirstDayOfMonth [ iMonth ] &&
					  iDayInYear <= g_riLastDayOfMonth  [ iMonth ] + 1 ) {
					m_cMonth = (unsigned char) (iMonth + 1);
					m_cDay   = (unsigned char) (iDayInYear - g_riFirstDayOfMonth [ iMonth ] + 1);
				}
			}
			else {
				if ( iDayInYear >= g_riFirstDayOfMonth [ iMonth ] + 1 &&
					  iDayInYear <= g_riLastDayOfMonth  [ iMonth ] + 1 ) {
					m_cMonth = (unsigned char) (iMonth + 1);
					m_cDay   = (unsigned char) (iDayInYear - g_riFirstDayOfMonth [ iMonth ]);
				}
			}
		}
		else {
			if ( iDayInYear >= g_riFirstDayOfMonth [ iMonth ] &&
				  iDayInYear <= g_riLastDayOfMonth  [ iMonth ] ) {
				m_cMonth = (unsigned char) (iMonth + 1);
				m_cDay   = (unsigned char) (iDayInYear - g_riFirstDayOfMonth [ iMonth ] + 1);
			}
		}
	}

   m_eDayOfWeek = (EWeekDay) 0;
   MDYToJulian( );
}

//////////////////////////////////////////////////////////////////////////////
// tDate Arithmetic

tDate tDate::operator++( int )
{
   tDate temp = *this;
   m_lJulian++;
   JulianToMDY();

   return temp;
}

//////////////////////////////////////////////////////////////////////////////
tDate tDate::operator--( int )
{
   tDate temp = *this;
   m_lJulian--;
   JulianToMDY();

   return temp;
}


//////////////////////////////////////////////////////////////////////////////
// Conversions
//////////////////////////////////////////////////////////////////////////////

tDate::operator double( void )   // tDate to Excel/VBA date, for structure see source
                                  // of constructor taking double
{
   double dExcelDate;

   DateToExcelDate( *this, dExcelDate );

   return dExcelDate;
}

//////////////////////////////////////////////////////////////////////////////
void tDate::JulianToMDY( void )
{
   int iMonth;
   int iDay;
   int iYear;

   JulianToMDY( m_lJulian, iMonth, iDay, iYear );

   m_cMonth = (unsigned char) iMonth;
   m_cDay   = (unsigned char) iDay;
   m_iYear  = (int)  			iYear;

   JulianToDOW( );
}


//////////////////////////////////////////////////////////////////////////////
// Static version, added on 30/04/1996 by JVdE
//
bool tDate::JulianToMDY( long lJulian, int& iMonth, int& iDay, int& iYear )
{
   long J = lJulian;
   
   long Z = J - 1L;
   
   long a, A, B, C, D, E;
   a = Div(100L * Z - 186721625L, 3652425L);
   A = (Z < 2299161L) ? Z : (Z + 1L + a - Div(a,4L));
   B = A + 1524L;
   C = Div(100L * B - 12210L, 36525L);
   D = Div(36525L * C, 100L);
   E = Div((B - D) * 10000L, 306001L);
   
   iDay   = (int) (B - D - Div(306001L * E, 10000L));
   iMonth = (int) ((E < 14L) ? (E - 1L) : (E - 13L));
   iYear  = (int) (iMonth > 2) ? (C - 4716L) : (C - 4715L);
   
   if ( iYear <= 0 ) iYear--;
   
	return IsValidDate( iMonth, iDay, iYear );
}

//////////////////////////////////////////////////////////////////////////////
void tDate::MDYToJulian( void )
{
   long lJulian;

   MDYToJulian( (int) m_cMonth, (int) m_cDay, (int) m_iYear, lJulian );

   m_lJulian = lJulian;

   JulianToDOW();
}

//////////////////////////////////////////////////////////////////////////////
static bool Gregorian( int iYear, int iMonth, int iDay )
{
   if ( iYear > 1582 ) return true;
   if ( iYear < 1582 ) return false;
   if ( iMonth > 10 ) return true;
   if ( iMonth < 10 ) return false;
   return ( iDay >= 15 );
}

//////////////////////////////////////////////////////////////////////////////
bool tDate::MDYToJulian( int iMonth, int iDay, int iYear, long& lJulian )
{
   bool fGregorian = Gregorian( iYear, iMonth, iDay );

   if ( iYear < 0 ) iYear++;
   if ( iMonth <= 2 ) {  iYear--; iMonth += 12; }

   long b = 0;
   if ( fGregorian ) {
      long a = iYear / 100;
      b = 2 - a + (a/4);
   }

   lJulian = Div(1461L * (long) iYear, 4L);
   lJulian += b + ( 306001L * ((long) iMonth + 1L) ) / 10000L + (long) iDay + 1720996L;

   return true;
}


//////////////////////////////////////////////////////////////////////////////
int tDate::GetSortableDate( void ) const
{
   return ( m_iYear * 100 + m_cMonth ) * 100 + m_cDay;
}

//////////////////////////////////////////////////////////////////////////////
// Miscellaneous Routines

//////////////////////////////////////////////////////////////////////////////
// Non-static version, calls static version with m_iYear member
bool tDate::IsLeapYear( void ) const
{
   return IsLeapYear( m_iYear );
}


// Static version, takes year as parameter
bool tDate::IsLeapYear( int iYear )
{
  // Adjust for BC
  if ( iYear < 0 )
    iYear++;

  // Leap years are divisible by 4, years ending in 00, which
  // are normal unless divisible by 400.
  return ( iYear % 100 ) ? ( iYear % 4 == 0 ) : ( iYear % 400 == 0 );
}

//////////////////////////////////////////////////////////////////////////////
bool tDate::IsDST( void ) const
{
    // Initialize start of DST and STD
    tDate dtTempDST( ( g_nStartDST >> 8 ), ( ( g_nStartDST << 8 ) >> 8 ), m_iYear ) ;
    tDate dtTempSTD( ( g_nStartSTD >> 8 ), ( ( g_nStartSTD << 8 ) >> 8 ), m_iYear ) ;

    dtTempDST += - dtTempDST.DayOfWeek() + 8;      // DST begins first Sunday in April
    dtTempSTD -= ( dtTempSTD.DayOfWeek() - 1 );    // STD begins last Sunday in October

    if ( m_lJulian >= dtTempDST.m_lJulian && m_lJulian < dtTempSTD.m_lJulian )
      return true;

    return false;
}

//////////////////////////////////////////////////////////////////////////////
bool tDate::SetDST( int iMonth, int iDay ) const
{
   if ( iMonth > 0 && iMonth < 13 && iDay > 0 && iDay < 32 ) {
      g_nStartDST = ( iMonth << 8 ) + iDay;
      return true;
   }

   return false;
}

//////////////////////////////////////////////////////////////////////////////
bool tDate::SetSTD( int iMonth, int iDay ) const
{
   if ( iMonth > 0 && iMonth < 13 && iDay > 0 && iDay < 32 ) {
      g_nStartSTD = (iMonth << 8) + iDay;
      return true;
   }

   return false;
}

//////////////////////////////////////////////////////////////////////////////
tm tDate::eom( void ) const
{
   tm tmDate;
   tDate dtDate( ( m_cMonth % 12 ) + 1, 1, m_iYear );

   if ( m_cMonth == 12 )
      dtDate.m_iYear++;

   dtDate--;

   tmDate.tm_year  = (int) dtDate.m_iYear;
   tmDate.tm_mon   = (int) dtDate.m_cMonth;
   tmDate.tm_wday  = (int) dtDate.DayOfWeek() - 1;
   tmDate.tm_mday  = (int) dtDate.m_cDay;
   tmDate.tm_hour  = 23;
   tmDate.tm_min   = 59;
   tmDate.tm_sec   = 59;
   tmDate.tm_yday  = dtDate.DayOfYear();
   tmDate.tm_isdst = (int) dtDate.IsDST();

   return tmDate;
}

//////////////////////////////////////////////////////////////////////////////
tm tDate::GetDate( void ) const
{
   tm tmdate;

	tmdate.tm_sec	 = (int) m_cSecond;
	tmdate.tm_min	 = (int) m_cMinute;
	tmdate.tm_hour	 = (int) m_cHour;
	tmdate.tm_mday	 = (int) m_cDay;
	tmdate.tm_mon	 = (int) m_cMonth;
	tmdate.tm_year	 = (int) m_iYear;
	tmdate.tm_wday	 = (int) DayOfWeek() - 1;
	tmdate.tm_yday	 = (int) DayOfYear();
	tmdate.tm_isdst = (int) IsDST();

   return tmdate;
}

//////////////////////////////////////////////////////////////////////////////
const tDate& tDate::SetToday()
{
	time_t now;
	tm     tmdate;
	
	time( &now );
	tmdate = *localtime( &now );

	m_cSecond = (unsigned char) tmdate.tm_sec;
	m_cMinute = (unsigned char) tmdate.tm_min;
	m_cHour   = (unsigned char) tmdate.tm_hour;
	m_cDay    = (unsigned char) tmdate.tm_mday;
	m_cMonth  = (unsigned char) tmdate.tm_mon;
	m_iYear   = (int)    		 tmdate.tm_year;

   MDYToJulian();

   return *this;
}

//////////////////////////////////////////////////////////////////////////////
tDate tDate::Today( void )
{
   tDate date;

   date.SetToday( );

   return date;
}


//////////////////////////////////////////////////////////////////////////////
const tDate& tDate::Set( int iMonth, int iDay, int iYear,
                         int iHour   /* = 0 */, 
                         int iMinute /* = 0 */, 
                         int iSecond /* = 0 */ )
{
   m_cMonth  = (unsigned char) iMonth;
   m_iYear   = iYear;
   m_cDay    = (unsigned char)( ( iDay < DaysInMonth() ) ? iDay : DaysInMonth() );
   m_cHour   = (unsigned char) ( max( 0, min( 23, iHour   ) ) );
   m_cMinute = (unsigned char) ( max( 0, min( 59, iMinute ) ) );
   m_cSecond = (unsigned char) ( max( 0, min( 59, iSecond ) ) );

   MDYToJulian();

   return *this;
}


//////////////////////////////////////////////////////////////////////////////
const tDate& tDate::SetYMD( int iYMDDate )
{
	if ( iYMDDate ) {
		m_cDay    = (unsigned char) (iYMDDate % 100);
		iYMDDate /= 100;
		m_cMonth  = (unsigned char) (iYMDDate % 100);
		iYMDDate /= 100;
		m_iYear   = iYMDDate;
		m_cHour   = 0;
		m_cMinute = 0;
		m_cSecond = 0;
	}
	else {
		m_cDay    = 0;
		m_cMonth  = 0;
		m_iYear   = 0;
		m_cHour   = 0;
		m_cMinute = 0;
		m_cSecond = 0;
	}

   MDYToJulian ( );

   return *this;
}

//////////////////////////////////////////////////////////////////////////////
const tDate& tDate::Set( int iDayInYear, int iYear )
{
   tDate HelpDate( iDayInYear, iYear );

   *this = HelpDate;

   return *this;
}

//////////////////////////////////////////////////////////////////////////////
int tDate::DaysInMonth() const
{
   return DaysInMonth( m_cMonth, m_iYear );
}


//////////////////////////////////////////////////////////////////////////////
int tDate::DaysInMonth( int iMonth, int iYear )
{
  int nDays;

	switch ( iMonth ) {
		case 2:
			nDays = 28 + IsLeapYear( iYear );
			break;

		case 4:
		case 6:
		case 9:
		case 11:
			nDays = 30;
			break;

		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			nDays = 31;
			break;

		default:
			// Invalid month...
			nDays = 0;
			break;
	}

  return nDays;
}

//////////////////////////////////////////////////////////////////////////////
const tDate & tDate::AddMonths( int iCount )
{
   unsigned char cDays;

   iCount += m_cMonth;

	while ( iCount < 1 ) {
		iCount += 12;
		m_iYear--;
	}

	while ( iCount > 12 ) {
		iCount -= 12;
		m_iYear++;
	}

   m_cMonth = (unsigned char) iCount;
   cDays = (unsigned char) DaysInMonth();

   // JVdE, 29/11/1996:
   // Commented this part out, reason:
   // 
   // Say we are on 31/1/xxxx and add 1 month:
   // This algorithm will put us on 2 or 3 March of the same year,
   // which is most likely not what was intended.
   //
   // What should really happen is the following:
   // If the day we end up on is past the last day of the month,
   // one should bump it back to the last day of the month.
   // 
   // Checked all occurrences of AddMonths in the whole of IRIS
   // and could not find any place where this (incorrect) behaviour
   // is relied upon (or acceptable)
   //

   if ( m_cDay > cDays )
      m_cDay = cDays;

   // JVdE, 29/11/1996: End of new implementation

   MDYToJulian();

   return *this;
}


//////////////////////////////////////////////////////////////////////////////
int tDate::GetDaysInYear( int iYear ) 
{ 
   return IsLeapYear( iYear ) ? 366 : 365; 
}


//////////////////////////////////////////////////////////////////////////////
// tDate::YearInFull. Take a year given in 2 digit or full 4 digit and convert
// to the full 4 digit form if in the short 2 digit form.

unsigned short tDate::YearInFull( unsigned short usYear )
{
   // in this case the year representation is assumed to already be full.
   if ( usYear > 99 ) return usYear;

	time_t now;
	tm     tmdate;
	
	time( &now );
	tmdate = *localtime( &now );

   // Rules for expansion of a 2 figure year value.
   unsigned short usCenturyNow  = (unsigned short) ( tmdate.tm_year / 100 );
   unsigned short usCenturyPrev = usCenturyNow;
   unsigned short usCenturyNext = usCenturyNow;

   if ( usYear < 50 )
      usCenturyPrev --;
   else
      usCenturyNext ++;

   // construct the 2 full year brackets
   unsigned short usYearFullPrev = (unsigned short) ( 100 * (int) usCenturyPrev + usYear );
   unsigned short usYearFullNext = (unsigned short) ( 100 * (int) usCenturyNext + usYear );

   // return the closest bracket
   return ( ( tmdate.tm_year - usYearFullPrev ) > ( usYearFullNext - tmdate.tm_year ) )
          ? usYearFullNext : usYearFullPrev;
}


//////////////////////////////////////////////////////////////////////////////
// Calculate the difference between 2 weekday numbers. If iDay0 > iDay1, it
// is assumed that iDay1 belongs to the following week.
int tDate::DayDiff( int iDay1, int iDay0 )
{
   int   iDiff = iDay1 - iDay0;
   return ( iDiff < 0 ) ? ( iDiff + 7 ) : iDiff;
}

//////////////////////////////////////////////////////////////////////////////
bool tDate::SetLastDayOfWeek( EWeekDay iWeekDay )
{
   bool fSet = true;

	if ( iWeekDay >= SUNDAY && iWeekDay <= SATURDAY ) {
		g_iLastDayOfWeek = (EWeekDay) iWeekDay;
	}
	else
      fSet = false;

   return fSet;
}

//////////////////////////////////////////////////////////////////////////////
tDate::EWeekDay tDate::GetLastDayOfWeek( void )
{
   return g_iLastDayOfWeek;
}

/////////////////////////////////////////////////////////////////////////////
tDate::EWeekDay tDate::GetFirstDayOfWeek( void )
{
   return (g_iLastDayOfWeek == SATURDAY) ? (SUNDAY) : (EWeekDay) ( (int) g_iLastDayOfWeek + 1 );
}



//////////////////////////////////////////////////////////////////////////////
// If the week belongs to the previous year, the value returned is a negative
// number of which the absolute value represents the final week number in the
// previous year. If the week belongs to the following year, the value returned
// is -1L.
int tDate::WeekOfYear() const
{
   return (int) ComputeWeekOfYear( (long) m_cDay,
                                   (long) m_cMonth,
                                   (long) m_iYear,
                                   (long) tDate::GetFirstDayOfWeek() );
}

//////////////////////////////////////////////////////////////////////////////
tDate tDate::EndOfWeek( EWeekDay eLastDayOfWeek ) const
{
   tDate dateTemp( *this );

   long lAddDays = (long) eLastDayOfWeek - (long) m_eDayOfWeek;

   if ( lAddDays < 0 )
      lAddDays += 7;

	if ( lAddDays ) {
		dateTemp.m_lJulian += lAddDays;

		dateTemp.JulianToMDY( );
	}

   return dateTemp;
}


//////////////////////////////////////////////////////////////////////////////
bool tDate::IsValid( void ) const
{
	return ( IsValidDate( m_cMonth, m_cDay, m_iYear ) &&
				m_cHour <= 23 && m_cMinute <= 59 && m_cSecond <= 59 );
}

//////////////////////////////////////////////////////////////////////////////
bool tDate::IsValidDate( int iMonth, int iDay, int iYear )
{
   return ( iYear != 0  &&
				iMonth >= 1 && iMonth <= 12 &&
				iDay >= 1   && iDay <= DaysInMonth( iMonth, iYear ) );
}


//////////////////////////////////////////////////////////////////////////////
tDate tDate::Next( EWeekDay eWeekDay )
{
   tDate dateNext( *this );

   dateNext ++;

   while ( dateNext.DayOfWeek( ) != eWeekDay )
      dateNext ++;

   return dateNext;
}


//////////////////////////////////////////////////////////////////////////////
tDate tDate::Previous( EWeekDay eWeekDay )
{
   tDate datePrev( *this );

   datePrev --;

   while ( datePrev.DayOfWeek() != eWeekDay )
      datePrev --;

   return datePrev;
}



//////////////////////////////////////////////////////////////////////////////////////////
// Time support
void tDate::AddSeconds( int iSeconds )
{
   int iSecsPerDay       = 60 * 60 * 24;
   bool fNegativeSeconds = ( iSeconds < 0 );
   
	while ( abs ( iSeconds ) >= iSecsPerDay ) {
		if ( fNegativeSeconds ) {
			m_lJulian --;
			iSeconds += iSecsPerDay;
		}
		else {
			m_lJulian ++;
			iSeconds -= iSecsPerDay;
		}

	}

	int iHour = (int) m_cHour;
	int iMin  = (int) m_cMinute;
	int iSec  = (int) m_cSecond + iSeconds;

	if ( iSec >= 60 || iSec < 0 ) {
		iMin += iSec / 60;
		iSec %= 60;
	}

	if ( iMin >= 60 || iMin < 0 ) {
		iHour += iMin / 60;
		iMin  %= 60;
	}

	if ( iHour >= 24 || iHour < 0 ) {
		if ( fNegativeSeconds )
			m_lJulian --;
		else
			m_lJulian ++;

		iHour %= 24;
	}

   m_cHour   = (unsigned char) iHour;
   m_cMinute = (unsigned char) iMin;
   m_cSecond = (unsigned char) iSec;

   JulianToMDY();
}


//////////////////////////////////////////////////////////////////////////////
void tDate::AddMinutes( int iMinutes )
{
   AddSeconds( iMinutes * 60 );
}


//////////////////////////////////////////////////////////////////////////////
void tDate::AddHours( int iHours )
{
   AddMinutes( iHours * 60 );
}


////////////////////////////////////////////////////////////////////////////////////////////
// Free functions
//
tDate* tDate::GetVectorDate( int iDate, int iFreq )
{
   int iYear, iPeriods;

	if ( iFreq == 366 ) {
		iYear = iDate / 1000;
		iPeriods = iDate % 1000;
	}
	else {
		iYear = iDate / 100;
		iPeriods = iDate % 100;
	}

   tDate* pDate = new tDate( 1, 1, iYear );
   if ( ! pDate )
      return NULL;

	switch ( iFreq ) {
		case 1:	// VF_ANNUAL
			{
				return pDate;
			}

		case 2:	// VF_HALFANNUAL
			{
				int iMonth = 6 * ( iPeriods - 1 );
				if ( iMonth )
					pDate -> AddMonths( iMonth );
				return pDate;
			}

		case 4:	// VF_QUARTERLY
			{
				int iMonth = 3 * ( iPeriods - 1 );
				if ( iMonth )
					pDate -> AddMonths( iMonth );
				return pDate;
			}

		case 12:	// VF_MONTHLY
			{
				int iMonth = iPeriods - 1;
				if ( iMonth )
					pDate -> AddMonths( iMonth );
				return pDate;
			}

		case 53:	// VF_WEEKLY
			{
				int iWeek = iPeriods - 1;
				if ( iWeek )
					pDate -> AddWeeks( iWeek );
				return pDate;
			}

		case 366: // VF_DAILY
			{
				int iDay = iPeriods - 1;
				if ( iDay )
					*pDate += iDay;
				return pDate;
			}

		case 0:	// VF_UNKNOWN
		default:
			delete pDate;
			return pDate;
	}
}

//////////////////////////////////////////////////////////////////////////////
tDate * tDate::GetNextVectorDate( tDate * pDate, int iFreq )
{
   switch ( iFreq ) {
      case 1:  // VF_ANNUAL
         pDate -> AddYears( 1 );
         break;

      case 2:  // VF_HALFANNUAL
         pDate -> AddMonths( 6 );
         break;

      case 4:  // VF_QUARTERLY
         pDate -> AddMonths( 3 );
         break;

      case 12: // VF_MONTHLY
         pDate -> AddMonths( 1 );
         break;

      case 53: // VF_WEEKLY
         pDate -> AddWeeks(  1 );
         break;

      case 366: // VF_DAILY
         *pDate += 1;
         break;

      case 0:  // VF_UNKNOWN
      default:
         return 0;
   }

	return pDate;
}

//////////////////////////////////////////////////////////////////////////////
void tDate::AddPeriodToDate( tDate& date, EPeriod period, int iCount )
{
	switch ( period ) {
		case eWeek:
			date.AddWeeks( iCount );
			break;

		case eMonth:
			date.AddMonths( iCount );
			break;

		case eQuarter:
			date.AddMonths( 3 * iCount );
			break;

		case eSemester:
			date.AddMonths( 6 * iCount );
			break;

		case eYear:
			date.AddYears( iCount );
			break;

		case eDay:
			date += iCount;
			break;

		default:
			date += iCount;
			break;
	}
}

//////////////////////////////////////////////////////////////////////////////
EPeriod tDate::GetFrequencyAsPeriod( int iFreq )
{
	switch ( iFreq ) {
		case 1:								// XVector::VF_ANNUAL
			return eYear;

		case 2:								// XVector::VF_HALFANNUAL
			return eSemester;

		case 4:								// XVector::VF_QUARTERLY:
			return eQuarter;

		case 12:								// XVector::VF_MONTHLY:
			return eMonth;

		case 53:								// XVector::VF_WEEKLY:
			return eWeek;

		case 366:							// XVector::VF_DAILY:
			return eDay;

		case 0:								// XVector::VF_UNKNOWN:
			return GetFrequencyAsPeriod( 366	/* XVector::VF_DAILY */ );

		default:
			return GetFrequencyAsPeriod( 366	/* XVector::VF_DAILY */ );
	}
}

//////////////////////////////////////////////////////////////////////////////
int tDate::GetNumberOfPeriods( const tDate& start, const tDate& end, EPeriod period )
{
	switch ( period ) {
		case eWeek:
			return ( end - start ) / 7;

		case eMonth:
			{
				int iStartMonth = start.Year() * 12 + start.Month() - 1;
				int iEndMonth = end.Year() * 12 + end.Month() - 1;
				return iEndMonth - iStartMonth;
			}

		case eQuarter:
			{
				int iStartQuarter = start.Year() * 4 + ( start.Month() - 1 ) % 3;
				int iEndQuarter = end.Year() * 4 + ( end.Month() - 1 ) % 3;
				return iEndQuarter - iStartQuarter;
			}

		case eSemester:
			{
				int iStartSemester = start.Year() * 2 + ( start.Month() - 1 ) % 6;
				int iEndSemester = end.Year() * 2 + ( end.Month() - 1 ) % 6;
				return iEndSemester - iStartSemester;
			}

		case eYear:
			return end.Year() - start.Year();

		case eDay:
		default:
			return end - start;

	}
}


//////////////////////////////////////////////////////////////////////////////
int tDate::GetCurrentCentury( void )
{
	time_t now;
	tm     tmdate;
	
	time( &now );
	tmdate = *localtime( &now );

   return (int) ( ( tmdate.tm_year / 100 ) * 100 );
}



////////////////////////////////////////////////////////////////////////////////////////
// Various static conversions


bool tDate::DateToSystemTime( const tDate& date, tm& tmdate )
{
	memset( &tmdate, '\0', sizeof( tmdate ) );

	if ( date.IsValid() ) {
		tmdate.tm_sec	  = (int) date.m_cSecond;
		tmdate.tm_min	  = (int) date.m_cMinute;
		tmdate.tm_hour  = (int) date.m_cHour;
		tmdate.tm_mday  = (int) date.m_cDay;
		tmdate.tm_mon	  = (int) date.m_cMonth;
		tmdate.tm_year  = (int) date.m_iYear;
		tmdate.tm_wday  = (int) date.DayOfWeek() - 1;
		tmdate.tm_yday  = (int) date.DayOfYear();
		tmdate.tm_isdst = (int) date.IsDST();

		return true;
	}

	return false;
}


//////////////////////////////////////////////////////////////////////////////
bool tDate::SystemTimeToDate( const tm& tmdate, tDate& date )
{
   date.Set( (int) tmdate.tm_mon,
             (int) tmdate.tm_mday,
             (int) tmdate.tm_year,
             (int) tmdate.tm_hour,
             (int) tmdate.tm_min,
             (int) tmdate.tm_sec );

   return date.IsValid();
}


//////////////////////////////////////////////////////////////////////////////
// Remark on structure of Excel dates
//
// Dates and times are stored internally as different parts of a real number.
// The value to the left of the decimal represents a date between January 1, 100 and December 31, 9999, 
// inclusive.  Negative values represent dates prior to December 30, 1899.
// The value to the right of the decimal represents a time between 0:00:00 and 23:59:59, inclusive.  
// Midday is represented by .5.

bool tDate::DateToExcelDate( const tDate& date, double& ExcelDate )
{
   if ( ! date.IsValid() )
      return false;

   tDate DecemberThirty1899( 12, 30, 1899 );

   int  iDayDiff = date - DecemberThirty1899;

   double dSecondsSinceMidnight = date.m_cHour;
   dSecondsSinceMidnight *= 60;
   dSecondsSinceMidnight += date.m_cMinute;
   dSecondsSinceMidnight *= 60;
   dSecondsSinceMidnight += date.m_cSecond;

   // There are 86400 seconds in a day
   double dSecondsPerDay = 24.0 * 60.0 * 60.0;

   ExcelDate = iDayDiff + ( dSecondsSinceMidnight / dSecondsPerDay );

   return true;
}


//////////////////////////////////////////////////////////////////////////////
bool tDate::ExcelDateToDate( const double& ExcelDate, tDate& date )
{
   // Cut the digits left of the decimal point out, this is the number
   // of days since December 30, 1899
   long lDaysSinceDecemberThirty1899 = (long) ExcelDate;     
                                        
   tDate DecemberThirty1899( 12, 30, 1899 );

   date  = DecemberThirty1899;
   date += lDaysSinceDecemberThirty1899;

   double dSeconds = ( ExcelDate - floor( ExcelDate ) );

   long  lSeconds = (long) ( dSeconds * 24.0 * 60.0 * 60.0 );

   date.m_cSecond = (unsigned char) ( lSeconds % 60 );
   lSeconds /= 60;
   date.m_cMinute = (unsigned char) ( lSeconds % 60 );
   lSeconds /= 60;
   date.m_cHour   = (unsigned char) lSeconds;  

   date.MDYToJulian();

   return date.IsValid();
}


//////////////////////////////////////////////////////////////////////////////
bool tDate::DateToEruDate( const tDate& rDate, int& iEruDate, int iFreq )
{
   tDate dateTemp( rDate );

   return tDate::AdjustedDateToEruDate( dateTemp, iEruDate, iFreq, true );
}


//////////////////////////////////////////////////////////////////////////////
bool tDate::AdjustedDateToEruDate( tDate& rDate, int& iEruDate, int iFreq, bool fStartOfPeriod /* = false */ )
{
   bool fEndOfPeriod = ! fStartOfPeriod;

	if ( ! rDate.IsValid() ) {
		iEruDate = -1;
		return false;
	}

	switch ( iFreq ) {
		case 1:		// VF_ANNUAL
			iEruDate = rDate.Year() * 100 + 1;
			if ( fEndOfPeriod )
				rDate = rDate.EndOfYear();
			else
				rDate	= rDate.BeginningOfYear();
			break;

		case 2:		// VF_HALFANNUAL
			iEruDate = rDate.Year() * 100 + rDate.Semester();
			if ( fEndOfPeriod )
				rDate = rDate.EndOfSemester();
			else
				rDate	= rDate.BeginningOfSemester();
			break;

		case 4:		// VF_QUARTERLY
			iEruDate = rDate.Year() * 100 + rDate.Quarter();
			if ( fEndOfPeriod )
				rDate = rDate.EndOfQuarter();
			else
				rDate	= rDate.BeginningOfQuarter();
			break;

		case 12:		 // VF_MONTHLY
			iEruDate = rDate.Year() * 100 + rDate.Month();
			if ( fEndOfPeriod )
				rDate = rDate.EndOfMonth();
			else
				rDate	= rDate.BeginningOfMonth();
			break;
			
		case 52:		 // ?? In case someone decides the year must only have 52 weeks
		case 53:		 // VF_WEEKLY
			{
				int iWeek = rDate.WeekOfYear();
				int iYear = rDate.Year();
				if ( iWeek < 0 ) {
					if ( iWeek == -1 )
						iYear ++;
					else
						iYear	--;
				}

				iEruDate = iYear * 100 + iWeek;

				if ( fEndOfPeriod )
					rDate = rDate.EndOfWeek();
				else
					rDate	= rDate.BeginningOfWeek();
			}
			break;
			
		case 365:	  // ?? In case someone decides the year must only have 365 days
		case 366:	  // VF_DAILY
			iEruDate = rDate.Year() * 1000 + rDate.DayOfYear();
			break;
			
		default:
			iEruDate = -1;
			return false;
	}

	return true;
}

/*
   
   Returns true if this date belongs to the 53rd week
   
*/

//////////////////////////////////////////////////////////////////////////////
bool tDate::EruDateToDate( int iEruDate, tDate& rDate, int iFreq, bool fStartOfPeriod )
{
   bool fIsValid = true;

	if ( iEruDate <= 0 ) {
		rDate.Clear();

		return false;
	}

	int iYear = iEruDate;
	int iPer  = 1;

	if ( iYear >= 0 && iYear < 100 ) {
		// Assume year only without century, period set to 1
      int iYearCentury     = iYear + tDate::GetCurrentCentury();
      int iYearCenturyPrev = iYearCentury - 100;
      int iYearCenturyNext = iYearCentury + 100;

      long lJulianNow;
      tDate today = tDate::Today();
      lJulianNow = today.m_lJulian;

      long lJulian;
      long lJulianPrev;
      long lJulianNext;

      tDate::MDYToJulian( 12, 31, iYearCentury, lJulian );
      tDate::MDYToJulian( 12, 31, iYearCenturyPrev, lJulianPrev );
      tDate::MDYToJulian( 1, 1, iYearCenturyNext, lJulianNext );

      // take closest to now
      lJulian -= lJulianNow;
      if ( lJulian < 0 ) lJulian = -lJulian;
      lJulianPrev -= lJulianNow;
      if ( lJulianPrev < 0 ) lJulianPrev = -lJulianPrev;
      lJulianNext -= lJulianNow;
      if ( lJulianNext < 0 ) lJulianNext = -lJulianNext;

      if ( lJulian < lJulianPrev && lJulian < lJulianNext )
         iYear = iYearCentury;
      else if ( lJulianPrev < lJulian && lJulianPrev < lJulianNext )
         iYear = iYearCenturyPrev;
      else
         iYear = iYearCenturyNext;
	}
	else if ( iYear <= 9999 ) {
		// Assume year only, period set to 1
	}
	else if ( iYear <= 999999 ) {
		// Assume year + subperiod ( 2 digits )
		iYear /= 100;
		iPer   = iEruDate % 100;
	}
	else if ( iYear <= 9999999 ) {
		// Assume year + subperiod ( 3 digits )
		iYear /= 1000;
		iPer   = iEruDate % 1000;
	}
	else if ( iYear <= 99999999 ) {
		// Assume YYYYMMDD
		rDate.Set( ( iEruDate % 10000 ) / 100, iEruDate % 100, iEruDate / 10000 );

		fIsValid = rDate.IsValid();

		return fIsValid;
	}
	else {
		// Error, invalid date, will be cleared
		iFreq = 0;
	}

	if ( iFreq == 52 )
		iFreq = 53;			// ?? In case someone decides the year must only have 52 weeks
	else if ( iFreq == 365 )
		iFreq = 366;		// ?? In case someone decides the year must only have 365 days

	switch ( iFreq ) {
		case 1:		// VF_ANNUAL
			if ( fStartOfPeriod )
				rDate.Set( 1, 1, iYear );
			else
				rDate.Set( 12, 31, iYear );
			break;

		case 2:		// VF_HALFANNUAL
			if ( iPer < 1 || iPer > 2 )
				fIsValid = false;
			else {
				rDate.Set( ( iPer - 1 ) * 6 + 1, 1, iYear );
				if ( ! fStartOfPeriod )
					rDate = rDate.EndOfSemester();
			}
			break;

		case 4:		// VF_QUARTERLY
			if ( iPer < 1 || iPer > 4 )
				fIsValid = false;
			else {
				rDate.Set( ( iPer - 1 ) * 3 + 1, 1, iYear );
				if ( ! fStartOfPeriod )
					rDate = rDate.EndOfQuarter();
			}
			break;

		case 12:		 // VF_MONTHLY
			if ( iPer < 1 || iPer > 12 )
				fIsValid = false;
			else {
				rDate.Set( iPer, 1, iYear );
				if ( ! fStartOfPeriod )
					rDate = rDate.EndOfMonth();
			}
			break;

		case 53:		 // VF_WEEKLY
			if ( iPer < 1 || iPer > 53 )
				fIsValid = false;
			else {
				bool fAskedForWeek53 = ( iPer == 53 );

				if ( fAskedForWeek53 ) {
					long nWeeks = ComputeWeeksInYear( (long) iYear,
																 (long) tDate::GetFirstDayOfWeek() );

					if ( nWeeks != 53L ) {
						iPer = 1;
						iYear ++;
					}
				}

				tDate tempDate( 1, 1, iYear );
				int iWeek;
				do {
					iWeek = tempDate.WeekOfYear();
					if ( iWeek > 0 ) break;
					tempDate += 7;
				} while ( iWeek < 0 );

				tempDate += ( iPer - 1 ) * 7;

				if ( ! fStartOfPeriod )
					tempDate += 6;

				rDate = tempDate;
			}
			break;
			
		case 366:	  // VF_DAILY
			if ( iPer < 1 || iPer > GetDaysInYear( iYear ) )
				fIsValid = false;
			else {
				tDate tempDate( iPer, iYear );

				rDate = tempDate;
			}
			break;
			
		default:
			fIsValid = false;
			break;
	}

	return fIsValid;
}

//////////////////////////////////////////////////////////////////////////////
bool tDate::IsInWeek53( void ) const
{
   if ( ! IsValid() )
      return false;

   int iWeek = WeekOfYear();
   return ( iWeek == 53 || iWeek == -53 );
}

//////////////////////////////////////////////////////////////////////////////
int tDate::WeeksInYear() const
{
   return (int) ComputeWeeksInYear( (long) m_iYear,
                                    (long) tDate::GetFirstDayOfWeek() );
}

//////////////////////////////////////////////////////////////////////////////
static void ToUpperCase( char* psz )
{
	while ( *psz )
		*psz = (char) toupper( (int) *psz );
}

//////////////////////////////////////////////////////////////////////////////
// Format a date according to certain rules:
// %d   : day number - minimum digits.
// %dd  : day number - always 2 digits.
// %ddd : shortened day of week (eg mon, tue, wed, ...)
// %Ddd : Shortened day of week (eg Mon, Tue, Wed, ...)
// %DDD : SHORTENED day of week (eg MON, TUE, WED, ...)
// %dddd: full day of week (eg monday, tuesday, ...)
// %Dddd: Full day of week (eg Monday, Tuesday, ...)
// %DDDD: FULL day of week (eg MONDAY, TUESDAY, ...)
// %o   : post fix for day number (eg st, nd, th, ...)
// %O   : post fix for day number (eg ST, ND, TH, ...)
// %m   : month number - minimum digits.
// %mm  : month number - always 2 digits.
// %mmm : shortened month name (eg jan, feb, ...)
// %Mmm : Shortened month name (eg Jan, Feb, ...)
// %MMM : SHORTENED month name (eg JAN, FEB, ...)
// %mmmm: full month name (eg january, february, ...)
// %Mmmm: Full month name (eg January, February, ...)
// %MMMM: FULL month name (eg JANUARY, FEBRUARY, ...)
// %yy  : short year (eg 97)
// %yyyy: full year (eg 1997)
// @h   : hour - minimum digits.
// @hh  : hour - always 2 digits.
// @Hh  : hour in 24 hour clock - always 2 digits.
// @m   : minute - minimum digits.
// @mm  : minute - always 2 digits.
// @s   : second - minimum digits.
// @ss  : second - always 2 digits.
// @p   : pm / am.
// @P   : PM / AM.
// %%   : '%' char.
// @@   : '@' char.
// ^    : void character (eg "@h^hr" -> "9hr" and "@^@h" -> "@9")
tLiteral& tDate::Format( tLiteral& strBuffer, const char* lpszFormat ) const
{
   static char* apszDays[]   = { NULL,
                                 "sunday", "monday", "tuesday", "wednesday", "thursday", "friday",
                                 "saturday" };
   static char* apszMonths[] = { NULL,
                                 "january", "february", "march", "april", "may", "june", "july",
                                 "august", "september", "october", "november", "december" };
   
   if ( ! IsValid() ) {
      strBuffer = "Invalid tDate object";
      return strBuffer;
   }
      
   strBuffer = "";
   
   char  szBuff[ 32 ];
   
   char* pchFormat = (char*) lpszFormat;
   
   while ( *pchFormat ) {
      if ( *pchFormat == '%' ) {
         pchFormat ++;
         if ( *pchFormat == '\0' ) {
            strBuffer += '%';
            break;
         }
         
         switch ( *pchFormat ) {
            case '%':
               strBuffer += '%';
               pchFormat ++;
               break;
               
            case 'd':
            case 'D':
               switch ( strspn( pchFormat, "Dd" ) ) {
                  case 1:
                     sprintf( szBuff, "%i", (int) m_cDay );
                     strBuffer += szBuff;
                     pchFormat ++;
                     break;
                  case 2:
                     sprintf( szBuff, "%02i", (int) m_cDay );
                     strBuffer += szBuff;
                     pchFormat += 2;
                     break;
                  case 3:
                     strncpy( szBuff, apszDays[ m_eDayOfWeek ], 3 );
                     if ( *pchFormat == 'D' ) {
                        if ( pchFormat[ 1 ] == 'D' )
                           ToUpperCase( szBuff );
                        else
                           szBuff[ 0 ] = (char) toupper( (int) szBuff[ 0 ] );
                     }
                     strBuffer += szBuff;
                     pchFormat += 3;
                     break;
                  case 4:
                  default:
                     strcpy( szBuff, apszDays[ m_eDayOfWeek ] );
                     if ( *pchFormat == 'D' ) {
                        if ( pchFormat[ 1 ] == 'D' )
                           ToUpperCase( szBuff );
                        else
                           szBuff[ 0 ] = (char) toupper( (int) szBuff[ 0 ] );
                     }
                     strBuffer += szBuff;
                     pchFormat += 4;
                     break;
               }
               break;
               
            case 'o':
            case 'O':
               switch ( m_cDay % 10 ) {
                  case 1:
                     strcpy( szBuff, "st" );
                     break;
                  case 2:
                     strcpy( szBuff, "nd" );
                     break;
                  case 3:
                     strcpy( szBuff, "rd" );
                     break;
                  default:
                     strcpy( szBuff, "th" );
                     break;
               }
               if ( *pchFormat == 'O' )
                  ToUpperCase( szBuff );
               strBuffer += szBuff;
               pchFormat ++;
               break;
               
            case 'm':
            case 'M':
               switch ( strspn( pchFormat, "Mm" ) ) {
                  case 1:
                     sprintf( szBuff, "%i", (int) m_cMonth );
                     strBuffer += szBuff;
                     pchFormat ++;
                     break;
                  case 2:
                     sprintf( szBuff, "%02i", (int) m_cMonth );
                     strBuffer += szBuff;
                     pchFormat += 2;
                     break;
                  case 3:
                     strncpy( szBuff, apszMonths[ m_cMonth ], 3 );
                     if ( *pchFormat == 'M' ) {
                        if ( pchFormat[ 1 ] == 'M' )
                           ToUpperCase( szBuff );
                        else
                           szBuff[ 0 ] = (char) toupper( (int) szBuff[ 0 ] );
                     }
                     strBuffer += szBuff;
                     pchFormat += 3;
                     break;
                  case 4:
                  default:
                     strcpy( szBuff, apszMonths[ m_cMonth ] );
                     if ( *pchFormat == 'M' ) {
                        if ( pchFormat[ 1 ] == 'M' )
                           ToUpperCase( szBuff );
                        else
                           szBuff[ 0 ] = (char) toupper( (int) szBuff[ 0 ] );
                     }
                     strBuffer += szBuff;
                     pchFormat += 4;
                     break;
               }
               break;
            
            case 'y':
            case 'Y':
            {
               int nY = (int) strspn( pchFormat, "Yy" );
               if ( nY < 4 ) {
                  int iYear = m_iYear - 100 * (m_iYear / 100);
                  sprintf( szBuff, "%02i", iYear );
                  strBuffer += szBuff;
                  pchFormat += nY;
               }
               else {
                  sprintf( szBuff, "%i", m_iYear );
                  strBuffer += szBuff;
                  pchFormat += 4;
               }
               break;
            }
            
            default:
               strBuffer += '%';
               break;
         }
      }
      else if ( *pchFormat == '@' ) {
         pchFormat ++;
         if ( *pchFormat == '\0' ) {
            strBuffer += '%';
            break;
         }
         
         switch ( *pchFormat ) {
            case 'h':
            case 'H':
            {
               int iHour = m_cHour;
               if (*pchFormat == 'h' && m_cHour > 12) iHour -= 12;
               pchFormat ++;
               if ( toupper( *pchFormat ) == 'H' ) {
                  sprintf( szBuff, "%02i", iHour );
                  strBuffer += szBuff;
                  pchFormat ++;
               }
               else {
                  sprintf( szBuff, "%i", iHour );
                  strBuffer += szBuff;
               }
               break;
            }
            
            case 'M':
            case 'm':
               pchFormat ++;
               if ( toupper( *pchFormat ) == 'M' ) {
                  sprintf( szBuff, "%02i", (int) m_cMinute );
                  strBuffer += szBuff;
                  pchFormat ++;
               }
               else {
                  sprintf( szBuff, "%i", (int) m_cMinute );
                  strBuffer += szBuff;
               }
               break;
               
            case 'S':
            case 's':
               pchFormat ++;
               if ( toupper( *pchFormat ) == 'S' ) {
                  sprintf( szBuff, "%02i", (int) m_cSecond );
                  strBuffer += szBuff;
                  pchFormat ++;
               }
               else {
                  sprintf( szBuff, "%i", (int) m_cSecond );
                  strBuffer += szBuff;
               }
               break;
            
            case 'P':
               if ( m_cHour >= 12 )
                  strBuffer += "PM";
               else
                  strBuffer += "AM";
               pchFormat ++;
               break;
               
            case 'p':
               if ( m_cHour >= 12 )
                  strBuffer += "pm";
               else
                  strBuffer += "am";
               pchFormat ++;
               break;
            
            case '@':
               strBuffer += '@';
               pchFormat ++;
               break;
         
            default:
               strBuffer += '@';
               break;
         }
      }
      else {
         if ( *pchFormat == '^' )
            pchFormat ++;
         else {
            strBuffer += *pchFormat;
            pchFormat ++;
         }
      }
   }
   return strBuffer;
}

//////////////////////////////////////////////////////////////////////////////
tDate tDate::EasterSunday( int iYear )
{
	int a = iYear / 100;
	int b = iYear % 100;
	int c = iYear % 19;
	int d = (19*c + a - a/4 - (a - (a + 8)/25 + 1)/3 + 15) % 30;
	int e = (32 + 2*(a % 4) + 2*(b / 4) - d - (b % 4)) % 7;
	int f = d + e - 7*((c + 11*d +22*e) / 451) + 114;
   return tDate( (f / 31), ((f % 31) + 1), iYear );
}

