/*
 *	tFileInfo.cpp
 */
 
#include "filehandle.h"
#include "misc.h"

#include "tfileinfo.h"


/*----------------------------------------------------------------------------
   GetFileSize
----------------------------------------------------------------------------*/
long tFileInfo::GetFileSize( const char* pszFilePath )
{
   tFileHandle FileHandle;
   FileHandle.OpenFileForRead( pszFilePath, tFileHandle::Text );
   return FileHandle.FileLength();
}

/*----------------------------------------------------------------------------
   CorrectDirSeparator
----------------------------------------------------------------------------*/
tString tFileInfo::CorrectDirSeparator( const char* pszFileOrPath )
{
   tString strFileOrPathCorrected;

   char szBuffer[ 512 ];
   strcpy( szBuffer, pszFileOrPath );

   char chEroneousSep, chCorrectSep;

   if ( DIR_SEPERATOR == '\\' ) {
      // replace forward slashes with backward slashes
      chEroneousSep = '/';
      chCorrectSep = '\\';
   }
   else {
      // replace backward slashes with formward slash
      chEroneousSep = '\\';
      chCorrectSep = '/';
   }

   char* pchFind = szBuffer;
   while ( pchFind = strchr( pchFind, chEroneousSep ) )
      *pchFind = chCorrectSep;

   strFileOrPathCorrected = szBuffer;
   return strFileOrPathCorrected;
}

/*----------------------------------------------------------------------------
   tFileInfo ctor - from Dir & FName
----------------------------------------------------------------------------*/
tFileInfo::tFileInfo( const char* pszFileDir, const char* pszFileName )
:  m_strFileName( (char*) pszFileName )
,  m_pFile( NULL )
{
   if ( pszFileDir && pszFileDir[0] )
      m_strFileDir = CorrectDirSeparator( pszFileDir );
   else
      m_strFileDir = ".";

   m_strFilePath = m_strFileDir + DIR_SEPERATOR + m_strFileName;
}

/*----------------------------------------------------------------------------
   tFileInfo ctor - from full Path
----------------------------------------------------------------------------*/
tFileInfo::tFileInfo( const char* pszFilePath )
:  m_pFile( NULL )
{
   char szBuffer[ 512 ];

   m_strFilePath = CorrectDirSeparator( pszFilePath );

   char* pszPath = (char*) m_strFilePath;
   for ( char* pchPath = pszPath + strlen( pszPath ) - 1; pchPath > pszPath; pchPath -- )
   {
      if ( *pchPath == DIR_SEPERATOR )
      {
         m_strFileName = &pchPath[ 1 ];
         size_t nPathLen = (size_t) (pchPath - pszPath);
         memcpy( szBuffer, pszPath, nPathLen );
         szBuffer[ nPathLen ] = '\0';
         m_strFileDir = szBuffer;
         break;
      }
   }
}

/*----------------------------------------------------------------------------
   dtor
----------------------------------------------------------------------------*/
tFileInfo::~tFileInfo()
{
   CloseFile();
}

/*----------------------------------------------------------------------------
   FileExists
----------------------------------------------------------------------------*/
bool tFileInfo::FileExists() const
{
   FILE* pFile = fopen( m_strFilePath, "r" );
   if ( ! pFile )
      return false;

   fclose( pFile );
   return true;
}

/*----------------------------------------------------------------------------
   DirectoryExists
----------------------------------------------------------------------------*/
bool tFileInfo::DirectoryExists() const
{
   int erc = 0;

#if defined( M_AIX ) || defined( M_GNU )
   {
      struct stat statBuffer;
      erc = stat( m_strFileDir, &statBuffer );
   }
#else
   {
      struct _stat statBuffer;
      erc = _stat( m_strFileDir, &statBuffer );
   }
#endif

   return ( erc > 0 ) ? false : true;
}

/*----------------------------------------------------------------------------
   CreateDirectory
----------------------------------------------------------------------------*/
void tFileInfo::CreateDirectory() const
{
   CreateDir( m_strFileDir );
}

/*----------------------------------------------------------------------------
   OpenFile - NB: do not fclose the returned file pointer. Call CloseFile()
----------------------------------------------------------------------------*/
FILE* tFileInfo::OpenFile( const char* pszMode )
{
   CloseFile();
   return m_pFile = fopen( m_strFilePath, pszMode );
}

/*----------------------------------------------------------------------------
   CloseFile
----------------------------------------------------------------------------*/
void tFileInfo::CloseFile()
{
   if ( ! m_pFile )
      return;

   fclose( m_pFile );
   m_pFile = NULL;
}

