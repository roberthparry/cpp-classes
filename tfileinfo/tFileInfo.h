/*
 *	tFileInfo.h
 */

#ifndef  _tFileInfo_h_
#define  _tFileInfo_h_

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _INC_STDIO
#  include <stdio.h>
#endif

#ifndef _TSTRING_H_
#  include "tstring.h"
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
class tFileInfo {
private:

   tString     m_strFileDir;
   tString     m_strFileName;
   tString     m_strFilePath;
   FILE*       m_pFile;

   static long GetFileSize( const char* pszFilePath );
   static tString CorrectDirSeparator( const char* pszFileOrPath );

public:

   tFileInfo( const char* pszFileDir, const char* pszFileName );
   tFileInfo( const char* pszFilePath );
  ~tFileInfo();
   
   inline const char* GetFileDir() const { return ( char* ) m_strFileDir; }
   inline const char* GetFileName() const { return ( char* ) m_strFileName; }
   inline const char* GetFilePath() const { return ( char* ) m_strFilePath; }
   inline long GetFileSize() const { return GetFileSize( ( char* ) m_strFilePath ); }
   bool FileExists() const;
   bool DirectoryExists() const;
   void CreateDirectory() const;
   FILE* OpenFile( const char* pszMode );
   FILE* GetFilePtr() const { return m_pFile; }
   void CloseFile();
};

#endif //_tFileInfo_h_