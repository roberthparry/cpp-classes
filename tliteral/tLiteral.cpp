/*
 *
 * tLiteral.cpp
 *
 */

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

#include "tliteral.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
inline static int MaxInt( int i, int j ) { return (i > j) ? i : j; }
inline static int MinInt( int i, int j ) { return (i < j) ? i : j; }
inline static int MaxUInt( unsigned int ui, unsigned int uj ) { return (ui > uj) ? ui : uj; }
inline static int MinUInt( unsigned int ui, unsigned int uj ) { return (ui < uj) ? ui : uj; }

//////////////////////////////////////////////////////////////////////////////////////////////////
void tLiteral::tLiteralValue::Init( const char* psz, int nsz )
{
   if ( psz ) {
      m_nsz = ( nsz == -1 ) ? strlen( psz ) : nsz;
      m_psz = (char*) malloc( m_nsz + 1 );
      memcpy( m_psz, psz, m_nsz );
      m_psz[ m_nsz ] = '\0';
   }
   m_nHash = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void tLiteral::tLiteralValue::Append( const char* psz )
{
   if ( psz ) {
      int nszExtra = strlen( psz ) + 1;
      m_psz = (char*) realloc( m_psz, m_nsz + nszExtra );
      memcpy( m_psz + m_nsz, psz, nszExtra );
      m_nsz += nszExtra - 1;
   }
   m_nHash = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void tLiteral::tLiteralValue::Append( char ch )
{
   m_psz = (char*) realloc( m_psz, m_nsz + 2 );
   m_psz[ m_nsz++ ] = ch;
   m_psz[ m_nsz ] = '\0';
   m_nHash = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void tLiteral::tLiteralValue::TrimLeft()
{
   for ( char* pch = m_psz; *pch; pch ++ ) {

      if ( *pch != ' ' && *pch != '\t' && *pch != '\r' && *pch != '\n' ) {

         if ( pch != m_psz ) {
            m_nsz -= (int) (pch - m_psz);
            memmove( m_psz, pch, (size_t) (m_nsz + 1) );
            m_nHash = 0U;
         }

         break;
      }
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void tLiteral::tLiteralValue::TrimRight()
{
   char* pchEnd = &m_psz[ m_nsz-1 ];

   for ( char* pch = pchEnd; ; pch -- ) {

      if ( *pch == ' ' || *pch == '\t' || *pch == '\r' || *pch == '\n' )
      {
         *pch = '\0';
      }
      else
      {
         if ( pch != pchEnd )
         {
            m_nsz -= (int) (pchEnd - pch);
            m_nHash = 0U;
         }
         break;
      }
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// duplicate or copy if buffer is large enough. fAllocated indicates whether allocation was necessary
static inline char* strdupcpy( const char* pszText, char* szTextBuffer, size_t cbTextBuffer, bool& fAllocated )
{
   size_t nszText = strlen( pszText );
   fAllocated = ( nszText >= cbTextBuffer );
   return ( fAllocated ) ? strdup( pszText ) : (char*) memcpy( szTextBuffer, pszText, nszText + 1 );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Case insensitive find.
static const char* strstri( const char* pszMain, const char* pszSearch )
{
   char szMain[ 512 ];
   char szSearch[ 256 ];
   bool fMainAlloc, fSearchAlloc;

   char* pszSearchLwr = strlwr( strdupcpy( pszSearch, szSearch, sizeof( szSearch ), fSearchAlloc ) );
   char* pszMainLwr = strlwr( strdupcpy( pszMain, szMain, sizeof( szMain ), fMainAlloc ) );
   char* pszMatch = strstr( pszMainLwr, pszSearchLwr );

   if ( pszMatch )
      pszMatch = (char*) (pszMain + (int) ( pszMatch - pszMainLwr ));

   if ( fSearchAlloc ) free( pszSearchLwr );
   if ( fMainAlloc   ) free( pszMainLwr );

   return pszMatch;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int tLiteral::tLiteralValue::Find( const char* pszSearch, bool fCaseSensitive ) const
{
   const char* pszMatch = ( fCaseSensitive ) ?
      strstr( m_psz, pszSearch ) : strstri( m_psz, pszSearch );

   return ( pszMatch ) ? (int) ( pszMatch - m_psz ) : -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void tLiteral::tLiteralValue::FindAndReplace( const char* pszSearch, const char* pszReplacement,
                                              bool fCaseSensitive, bool fReplaceAll )
{
   if ( ! strcmp( pszSearch, pszReplacement ) )
      return;
 
   typedef const char* (* PFN_STRSTR) ( const char*, const char* );
   PFN_STRSTR pfnStrStr = ( fCaseSensitive ) ? (PFN_STRSTR) strstr : (PFN_STRSTR) strstri;

   const char* pszMatch = pfnStrStr( m_psz, pszSearch );

   if ( ! pszMatch )
      return;

   int nSearch = strlen( pszSearch );
   int nReplacement = strlen( pszReplacement );
   int nSizeIncrement = nReplacement - nSearch;

   int nsz = m_nsz;
   char* psz = m_psz;

   int nAlloc = m_nsz + 1;
   int nAllocIncr = MaxInt( 32, 32*(( nSizeIncrement + 31 )/32) );

   do {
      nsz += nSizeIncrement;
      if ( nsz + 1 > nAlloc ) {
         nAlloc += nAllocIncr;
         psz = (char*) realloc( psz, nAlloc );
         if ( psz != m_psz )
            pszMatch = psz + (int) ( pszMatch - m_psz );
      }

      if ( nSizeIncrement > 0 )
         memmove( (void*) (pszMatch + nSizeIncrement), (const void*) pszMatch, (size_t) (m_nsz + 1 - (int) (pszMatch - psz)) );
      else if ( nSizeIncrement < 0 )
         memmove( (void*) pszMatch, (const void*) (pszMatch - nSizeIncrement), (size_t) (m_nsz + 1 - (int) (pszMatch - psz)) );

      memcpy( (void*) pszMatch, (const void*) pszReplacement, (size_t) nReplacement );

      m_nsz = nsz;
      m_psz = psz;

      if ( ! fReplaceAll )
         break;

   } while ( (pszMatch = pfnStrStr( pszMatch + nReplacement, pszSearch )) != NULL );

   m_psz = (char*) realloc( m_psz, m_nsz+1 );

   m_nHash = 0U;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void tLiteral::tLiteralValue::Split( const char* pszDelimiters, tArray< tLiteral >& arrTokens ) const
{
   char* pchPos = (char*) m_psz;

   arrTokens.RemoveAll();

   do {
      while ( *pchPos && strchr( pszDelimiters, *pchPos ) )
         pchPos ++;

      if ( ! *pchPos )
         break;
         
      char* pchPosNext = m_psz + m_nsz;

      for ( char* pchDelimiter = (char*) pszDelimiters; *pchDelimiter; pchDelimiter ++ ) {
         char* pch = strchr( pchPos, *pchDelimiter );
         if ( pch && pchPosNext > pch )
            pchPosNext = pch;
      }

      arrTokens.Add( tLiteral( pchPos, (int) (pchPosNext - pchPos) ) );

      pchPos = pchPosNext;

   } while ( pchPos );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
unsigned int tLiteral::tLiteralValue::HashKey()
{
   if ( m_nHash == 0 ) {
	   for ( char* pch = m_psz; *pch; pch++ )
		   m_nHash = (m_nHash << 5) + m_nHash + *pch;
   }
   return m_nHash;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
tLiteral::tLiteralValuePtr& tLiteral::tLiteralValuePtr::operator=( const tLiteral::tLiteralValuePtr& ValuePtr )
{
   if ( m_pValue != ValuePtr.m_pValue ) {
      if ( m_pValue ) m_pValue->RemoveReference();
      m_pValue = ValuePtr.m_pValue;
      Init();
   }
   return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
tLiteral::tCharProxy& tLiteral::tCharProxy::operator=( const tLiteral::tCharProxy& charproxy )
{
   m_str.StopSharing();
   m_str.m_ValuePtr->m_psz[ m_index ] = charproxy.m_str.m_ValuePtr->m_psz[ charproxy.m_index ];
   return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
tLiteral::tCharProxy& tLiteral::tCharProxy::operator=( char ch )
{
   m_str.StopSharing();
   m_str.m_ValuePtr->m_psz[ m_index ] = ch;
   return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
char* tLiteral::tCharProxy::operator&()
{
   m_str.StopSharing();
   m_str.m_ValuePtr->MarkUnshareable();
   return &(m_str.m_ValuePtr->m_psz[ m_index ]);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void tLiteral::StopSharing()
{  
   if ( m_ValuePtr->IsShared() ) {
      m_ValuePtr->RemoveReference();
      tLiteralValue* pLiteralValue = new tLiteralValue( m_ValuePtr->m_psz );
      m_ValuePtr = pLiteralValue;
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
tLiteral tLiteral::CreateFormatV( const char* pszFormat, va_list argList )
{
   tLiteral lit;
   lit.FormatV( pszFormat, argList );
   return lit;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
tLiteral tLiteral::CreateFormat( const char* pszFormat, ... )
{
   va_list argList;
   va_start( argList, pszFormat );
   return CreateFormatV( pszFormat, argList );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// printf-like formatting using passed string
tLiteral& tLiteral::Format( const char* pszFormat, ... )
{
	va_list argList;
	va_start( argList, pszFormat );

	return FormatV( pszFormat, argList );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// printf-like formatting using variable arguments parameter
tLiteral& tLiteral::FormatV( const char* pszFormat, va_list argList )
{
	va_list argListSave = argList;

	// make a guess at the maximum length of the resulting string
	int nMaxLen = 0;
	for ( const char* psz = pszFormat; *psz; psz ++ ) {
		// handle '%' character, but watch out for '%%'
		if ( *psz != '%' || *( ++psz ) == '%' ) {
			nMaxLen ++;
			continue;
		}

		int nItemLen = 0;

		// handle '%' character with format
		int nWidth = 0;
		for ( ; *psz; psz ++ ) {
			// check for valid flags
			if ( *psz == '#' )
				nMaxLen += 2;	 // for '0x'
			else if ( *psz == '*' )
				nWidth = va_arg( argList, int );
			else if ( *psz == '-' || *psz == '+' || *psz == '0' || *psz == ' ' )
				;
			else // hit non-flag character
				break;
		}
		// get width and skip it
		if ( nWidth == 0 ) {
			// width indicated by
			nWidth = atoi( psz );
			for ( ; *psz && isdigit( *psz ); psz ++ )
				;
		}

		int nPrecision = 0;
		if ( *psz == '.' ) {
			// skip past '.' separator (width.precision)
			psz ++;

			// get precision and skip it
			if ( *psz == '*' ) {
				nPrecision = va_arg( argList, int );
				psz ++;
			}
			else {
				nPrecision = atoi( psz );
				for ( ; *psz && isdigit( *psz ); psz ++ )
					;
			}
		}

		// should be on type modifier or specifier
		int nModifier = 0;
		if ( strncmp( psz, "I64", 3 ) == 0 ) {
			psz += 3;
			nModifier = 0x40000;
		}
		else {
			switch ( *psz ) {
				// modifiers that affect size
				case 'h':
					nModifier = 0x10000;
					psz ++;
					break;
				case 'l':
					nModifier = 0x20000;
					psz ++;
					break;

					// modifiers that do not affect size
				case 'F':
				case 'N':
				case 'L':
					psz ++;
					break;
			}
		}

		// now should be on specifier
		switch ( *psz | nModifier ) {
			// single characters
			case 'c':
			case 'C':
				nItemLen = 2;
				va_arg( argList, unsigned short );
				break;
			case 'c'|0x10000:
			case 'C'|0x10000:
				nItemLen = 2;
				va_arg( argList, char );
				break;
			case 'c'|0x20000:
			case 'C'|0x20000:
				nItemLen = 2;
				va_arg( argList, unsigned short );
				break;

				// strings
			case 's':
				{
					const char* pchNextArg = va_arg( argList, const char* );
               if ( pchNextArg )
						nItemLen = MaxInt( 1, strlen( pchNextArg ) );
               else
						nItemLen = 6;	// "(null)"
				}
				break;

			case 'S':
				{
					unsigned short* pchNextArg = va_arg( argList, unsigned short* );
               if ( pchNextArg )
						nItemLen = MaxInt( 1, wcslen( (const wchar_t*) pchNextArg ) );
					else
						nItemLen = 6;	// "(null)"
				}
				break;

			case 's'|0x10000:
			case 'S'|0x10000:
				{
					char* pchNextArg = va_arg( argList, char* );
               if ( pchNextArg )
						nItemLen = MaxInt( 1, strlen( pchNextArg ) );
               else
						nItemLen = 6; // "(null)"
				}
				break;

			case 's'|0x20000:
			case 'S'|0x20000:
				{
					unsigned short* pchNextArg = va_arg( argList, unsigned short* );
               if ( pchNextArg )
						nItemLen = MaxInt( 1, wcslen( (const wchar_t*) pchNextArg ) );
					else
						nItemLen = 6; // "(null)"
				}
				break;
		}

		// adjust nItemLen for strings
		if ( nItemLen ) {
         nItemLen = ( nPrecision ) ? MinInt( nItemLen, nPrecision ) : MaxInt( nItemLen, nWidth );
		}
		else {
			switch ( *psz ) {
				// integers
				case 'd':
				case 'i':
				case 'u':
				case 'x':
				case 'X':
				case 'o':
					if ( nModifier & 0x40000 )
						va_arg( argList, __int64 );
					else
						va_arg( argList, int );
					nItemLen = MaxInt( 32, nWidth + nPrecision );
					break;

				case 'e':
				case 'g':
				case 'G':
					va_arg( argList, double );
					nItemLen = MaxInt( 128, nWidth + nPrecision );
					break;

				case 'f':
					va_arg( argList, double );
					nItemLen = MaxInt( 128, 312 + nPrecision );
					break;

				case 'p':
					va_arg( argList, void* );
					nItemLen = MaxInt( 32, nWidth+nPrecision );
					break;

					// no output
				case 'n':
					va_arg( argList, int* );
					break;

				default:
               // TODO: place debug break here!
					break;
			}
		}

		// adjust nMaxLen for output nItemLen
		nMaxLen += nItemLen;
	}

	char szBuffer[ 1024 ];
	char* pszBuffer = szBuffer;

	if ( nMaxLen >= 1024 )
		pszBuffer = new char[ nMaxLen + 1 ];

	vsprintf( pszBuffer, pszFormat, argListSave );

   StopSharing();
	*this = pszBuffer;

	if ( nMaxLen >= 1024 )
		delete [] pszBuffer;

	va_end( argListSave );

	return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
tLiteral& tLiteral::operator+=( const tLiteral& str )
{
   StopSharing();
   m_ValuePtr->Append( str.m_ValuePtr->m_psz );
   return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
tLiteral& tLiteral::operator+=( const char* psz )
{
   StopSharing();
   m_ValuePtr->Append( psz );
   return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
tLiteral& tLiteral::operator+=( char ch )
{
   StopSharing();
   m_ValuePtr->Append( ch );
   return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// return nCount characters starting at zero-based nFirst
tLiteral tLiteral::Mid( int nFirst, int nCount ) const
{
   if ( nCount <= 0 )
      return tLiteral( "" );

   if ( nFirst <= 0 )
      return Left( nCount );

   if ( nCount > m_ValuePtr->m_nsz - nFirst )
      return Mid( nFirst );
   
   return tLiteral( m_ValuePtr->m_psz + nFirst, nCount );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// return all characters starting at zero-based nFirst
tLiteral tLiteral::Mid( int nFirst ) const
{
   if ( nFirst <= 0 )
      return tLiteral( m_ValuePtr->m_psz, m_ValuePtr->m_nsz );
   
   if ( nFirst >= m_ValuePtr->m_nsz )
      return tLiteral( "" );

   return tLiteral( m_ValuePtr->m_psz + nFirst, m_ValuePtr->m_nsz - nFirst );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// return first nCount characters in string
tLiteral tLiteral::Left( int nCount ) const
{
   if ( nCount <= 0 )
      return tLiteral( "" );

   if ( nCount >= m_ValuePtr->m_nsz )
      return tLiteral( m_ValuePtr->m_psz, m_ValuePtr->m_nsz );

   return tLiteral( m_ValuePtr->m_psz, nCount );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// return nCount characters from end of string
tLiteral tLiteral::Right( int nCount ) const
{
   if ( nCount <= 0 )
      return tLiteral( "" );

   if ( nCount >= m_ValuePtr->m_nsz )
      return tLiteral( m_ValuePtr->m_psz, m_ValuePtr->m_nsz );

   return tLiteral( m_ValuePtr->m_psz + m_ValuePtr->m_nsz - nCount, nCount );
}