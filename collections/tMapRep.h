/*
 *
 * tMapRep.h
 *
 */

#ifndef  _tMapRep_h_
#define  _tMapRep_h_

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef  _INC_NEW
   #include <new.h>
#endif

#ifndef  _Primes_h_
   #include "tprimes.h"
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef null
#define null 0
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef  POSITION_DEFINED
   #define  POSITION_DEFINED
   typedef struct _POSITION {} *POSITION;
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
#if !defined(M_AIX) && !defined(M_GNU)
#  define MUTABLE   mutable
#else
#  define MUTABLE
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
// CLASS: tMapRep< KEY_TYPE, VAL_TYPE > //////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE> class tMapRep {
private:

   enum {
      Load_Factor    = 50,                // 50% Load Factor.
      Initial_Size   = 17                 // Initial hash table size.
   };

private:

   //////////////////////////////////////////////////////////////////////////////////////////////////
   // CLASS: tNode //////////////////////////////////////////////////////////////////////////////////
   class tNode {
   public:
      tNode*         m_pNodeNext;         // Next node ( for collisions )
      unsigned int   m_uiHashTableIndex;  // Index in the Hash Table.
      unsigned int   m_uiHashKey;         // Keep this for rehashing purposes
                                                    // - avoids having to recompute it.
      KEY_TYPE       m_key;               // The key item.
      VAL_TYPE       m_value;                   // The value item.

      tNode( const KEY_TYPE& key, const VAL_TYPE& value )
         : m_key( key ), m_value( value ) {}
   };

   ///////////////////////////////////////////////////////////////////////////////////////////////
   // STRUCT: tBlock /////////////////////////////////////////////////////////////////////////////
   struct tBlock {
      tBlock*        m_pBlockNext;        // link to next tBlock.
      /*unsigned char* m_abNodeHeap[];*/  // m_nBlockSize nodes.
   };

    unsigned int      m_nHashTableSize;    // Size of the table.
    unsigned int      m_nCount;            // Number of items stored in the map.
    unsigned int      m_nMaxCount;         // Don't allow Load factor to be exceeded.
    tNode**           m_ppHashTable;       // The hash table.

   // Use tMapRep::NewNode to get the next available node to use and tMapRep::DeleteNode to dispose.
   // A tBlock is a structure with a pointer to the next structure. Allocation will allocate
   // one tBlock structure plus nBlockSize tMapNodes plus space for the m_keys and m_values.
   unsigned int      m_nBlockSize;        // Default is 32.
   tNode*            m_pNextAvailNode;    // Next available slot for new tNode.
   tBlock*           m_pBlocks;           // The chained Blocks.

   // cached data
   MUTABLE unsigned  m_cbNode;            // memory to allocate for node & element
   MUTABLE unsigned  m_cbBlock;           // memory to allocate for whole block

private:

   tNode*            NewNode              ( const KEY_TYPE& key, const VAL_TYPE& value, unsigned uiHashKey, unsigned uiHashIndex  );
   void              DeleteNode           ( tNode* pNode );
   void              BlockFree            ( tBlock* pBlocks );

   void              SetNewNodeAt         ( unsigned int uiIndex, unsigned int uiHashKey, const KEY_TYPE& key, const VAL_TYPE& value );
   tNode*            FindKey              ( const KEY_TYPE& key, tNode*& pNodePrev, unsigned int& uiIndex, unsigned int& uiHashKey ) const;

   void              Rehash               ();

   void              DestructList         ( tNode* pNode );

public:

   class tValueProxy {
   private:
      tMapRep<KEY_TYPE,VAL_TYPE>&   m_map;
      const KEY_TYPE&               m_key;

   public:

      tValueProxy( tMapRep<KEY_TYPE,VAL_TYPE>& map, const KEY_TYPE& key ) : m_map( map ), m_key( key ) {}

      tValueProxy& operator=( const tValueProxy& value )
      {  m_map.SetAt( m_key, ( VAL_TYPE& ) value );
         return *this;
      }

      tValueProxy& operator=( const VAL_TYPE& value )
      {  m_map.SetAt( m_key, value );
         return *this;
      }

      operator VAL_TYPE&()
      {  return ( ( tNode* ) m_map.Lookup( m_key ) )->m_value;
      }

   };

   friend class tValueProxy;

public:

                    ~tMapRep              ();
                     tMapRep              ( unsigned int nBlockSize = 32 );
                     tMapRep              ( const tMapRep<KEY_TYPE,VAL_TYPE>& map );
                                          
   void              InitHashTable        ( unsigned int nHashTableSize );
                                          
   POSITION          Lookup               ( const KEY_TYPE& key ) const;
   POSITION          GetFirst             () const;
   POSITION          GetNext              ( POSITION pos ) const;
                                          
   void              SetAt                ( const KEY_TYPE& key, const VAL_TYPE& value );
   const KEY_TYPE&   GetKeyAt             ( POSITION pos ) const;
   const VAL_TYPE&   GetValueAt           ( POSITION pos ) const;
   VAL_TYPE&         GetValueAt           ( POSITION pos );
   bool              RemoveKey            ( const KEY_TYPE& key );
   void              RemoveAll            ();
                                          
   unsigned int      GetHashTableSize     () const;
   unsigned int      GetCount             () const;
                                          
   tMapRep<KEY_TYPE,VAL_TYPE>&   operator=   ( const tMapRep<KEY_TYPE,VAL_TYPE>& map );

   tValueProxy                   operator[]  ( const KEY_TYPE& key ) const;
   tValueProxy                   operator[]  ( const KEY_TYPE& key );
};

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE>
inline unsigned int HashKey( const KEY_TYPE& key )
{
    // default identity hash - works for most primitive values
    unsigned int nHash = 0;
    
   const char* pch = ( char* ) &key;
   for ( int index = 0; index < sizeof( KEY_TYPE ); index ++, pch++ )
        nHash = ( nHash << 5 ) + nHash + *pch;

    return nHash;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
tMapRep<KEY_TYPE,VAL_TYPE>::tNode* tMapRep<KEY_TYPE,VAL_TYPE>::NewNode( const KEY_TYPE& key,
                                                                        const VAL_TYPE& value,
                                                                        unsigned uiHashKey,
                                                                        unsigned uiHashIndex )
{
   tNode* pNode;

   if ( m_pNextAvailNode == null ) {
      tBlock* pBlock = ( tBlock* ) malloc( m_cbBlock );
      ::new( ( void* ) pBlock ) tBlock();

      // chain the memory blocks together.
      pBlock->m_pBlockNext = m_pBlocks;
      m_pBlocks = pBlock;

      unsigned char* pbNodeHeap = ( unsigned char* )( pBlock + 1 );
      pbNodeHeap += ( m_nBlockSize - 1 ) * m_cbNode;

      for ( int iBlock = (int) m_nBlockSize - 1; iBlock >= 0; iBlock--, pbNodeHeap -= m_cbNode ) {
         pNode = ( tNode* ) pbNodeHeap;
         pNode->m_pNodeNext = m_pNextAvailNode;
         m_pNextAvailNode = pNode;
      }
   }

   pNode = m_pNextAvailNode;
   m_pNextAvailNode = m_pNextAvailNode->m_pNodeNext;

   ::new( ( void* ) pNode ) tNode( key, value );
   pNode->m_uiHashTableIndex = uiHashIndex;
   pNode->m_uiHashKey = uiHashKey;

   m_nCount ++;

   return pNode;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
void tMapRep<KEY_TYPE,VAL_TYPE>::DeleteNode( tMapRep<KEY_TYPE,VAL_TYPE>::tNode* pNode )
{
   pNode->m_key.~KEY_TYPE();
   pNode->m_value.~VAL_TYPE();
   pNode->m_pNodeNext = m_pNextAvailNode;
   m_pNextAvailNode = pNode;
   m_nCount --;

   if ( m_nCount == 0 )
      RemoveAll();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
void tMapRep<KEY_TYPE,VAL_TYPE>::BlockFree( tMapRep<KEY_TYPE,VAL_TYPE>::tBlock* pBlocks )
{
   if ( pBlocks ) {
      BlockFree( pBlocks->m_pBlockNext );
      free( pBlocks );
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
void tMapRep<KEY_TYPE,VAL_TYPE>::SetNewNodeAt( unsigned int uiIndex, unsigned int uiHashKey, const KEY_TYPE& key, const VAL_TYPE& value )
{
   tNode* pNodeNew = NewNode( key, value, uiHashKey, uiIndex );
   pNodeNew->m_pNodeNext = m_ppHashTable[ uiIndex ];
   m_ppHashTable[ uiIndex ] = pNodeNew;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
tMapRep<KEY_TYPE,VAL_TYPE>::tNode* tMapRep<KEY_TYPE,VAL_TYPE>::FindKey( const KEY_TYPE& key, tMapRep<KEY_TYPE,VAL_TYPE>::tNode*& pNodePrev, unsigned int& uiIndex, unsigned int& uiHashKey ) const
{
   uiIndex = HashKey( key );
   uiHashKey = uiIndex;
   uiIndex %= m_nHashTableSize;

   tNode* pNode = m_ppHashTable[ uiIndex ];
   if ( pNode == null ) return null;

   pNodePrev = null;

   for ( ; pNode != null; pNode = pNode->m_pNodeNext ) {
      if ( pNode->m_key == key )
         break;

      pNodePrev = pNode;
   }

   return pNode;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline tMapRep<KEY_TYPE,VAL_TYPE>::~tMapRep()
{
   RemoveAll();

   if ( m_ppHashTable ) {
      delete [] m_ppHashTable;
      m_ppHashTable = null;
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline tMapRep<KEY_TYPE,VAL_TYPE>::tMapRep( unsigned int nBlockSize )
:  m_nCount          ( 0 )
,  m_nHashTableSize  ( Initial_Size )
,  m_nMaxCount       ( ( Initial_Size * Load_Factor ) / 100 )
,  m_ppHashTable     ( new tNode*[ Initial_Size ] )
,  m_nBlockSize      ( nBlockSize )
,  m_pNextAvailNode  ( null )
,  m_pBlocks         ( null )
,  m_cbNode          ( sizeof( tNode ) + sizeof( KEY_TYPE ) + sizeof( VAL_TYPE ) )
,  m_cbBlock         ( sizeof( tBlock ) + m_nBlockSize * m_cbNode )
{
   memset( m_ppHashTable, 0, m_nHashTableSize * sizeof(tNode*) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
tMapRep<KEY_TYPE,VAL_TYPE>::tMapRep( const tMapRep<KEY_TYPE,VAL_TYPE>& map )
:  m_nCount          ( 0 )
,  m_nHashTableSize  ( map.m_nHashTableSize )
,  m_nMaxCount       ( map.m_nMaxCount )
,  m_ppHashTable     ( new tNode*[ map.m_nHashTableSize ] )
,  m_nBlockSize      ( map.m_nBlockSize )
,  m_pNextAvailNode  ( null )
,  m_pBlocks         ( null )
,  m_cbNode          ( map.m_cbNode )
,  m_cbBlock         ( map.m_cbBlock )
{
   unsigned int nCount = 0;
   memset( m_ppHashTable, 0, m_nHashTableSize * sizeof(tNode*) );

   for ( POSITION pos = map.GetFirst(); pos != null && nCount < map.m_nCount; pos = map.GetNext( pos ), nCount++ )
      SetNewNodeAt( ( ( tNode* ) pos )->m_uiHashTableIndex,
                          ( ( tNode* ) pos )->m_uiHashKey,
                          ( ( tNode* ) pos )->m_key,
                          ( ( tNode* ) pos )->m_value );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
void tMapRep<KEY_TYPE,VAL_TYPE>::InitHashTable( unsigned int nHashTableSize )
{
   if ( nHashTableSize < 1 )
      return;

   nHashTableSize = tPrimes::NextPrime( nHashTableSize );

   m_nHashTableSize = nHashTableSize;
   m_nMaxCount = ( nHashTableSize * Load_Factor ) / 100;  // use load factor.
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
void tMapRep<KEY_TYPE,VAL_TYPE>::Rehash()
{
   unsigned int nNewHashTableSize = tPrimes::NextPrime( m_nHashTableSize << 1 );

   tNode** ppNewHashTable = new tNode*[ nNewHashTableSize ];
   if ( ppNewHashTable == null ) return;

   memset( ppNewHashTable, 0, nNewHashTableSize * sizeof( tNode* ) );

   // Now iterate through the nodes of the old hash table placing them into the new hash table.
   tNode* pNode;
   tNode* pNodeNext;

   unsigned int nCount = 0;
   for ( unsigned int uiIndex = 0; uiIndex < m_nHashTableSize && nCount < m_nCount; uiIndex ++ )
   {
      if ( ( pNode = m_ppHashTable[ uiIndex ] ) != null ) {
         do {
            // change the nodes hash table index
            pNode->m_uiHashTableIndex = pNode->m_uiHashKey % nNewHashTableSize;

            // get the next node before it changes
            pNodeNext = pNode->m_pNodeNext;

            // change next node to the node at the first collision or null
            // if there is no collision.
            pNode->m_pNodeNext = ppNewHashTable[ pNode->m_uiHashTableIndex ];

            // slot the current node into the hash table.
            ppNewHashTable[ pNode->m_uiHashTableIndex ] = pNode;

            nCount ++;

         } while ( ( pNode = pNodeNext ) != null );
      }
   }

   // Every node has now been placed into pNewHashTable
   // - now free the old one and set it to the new.
   delete [] m_ppHashTable;
   m_ppHashTable = ppNewHashTable;
   m_nHashTableSize = nNewHashTableSize;
   m_nMaxCount = ( nNewHashTableSize * Load_Factor ) / 100; // use load factor.
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline void tMapRep<KEY_TYPE,VAL_TYPE>::DestructList( tMapRep<KEY_TYPE,VAL_TYPE>::tNode* pNode )
{  
   if ( pNode->m_pNodeNext )
      DestructList( pNode->m_pNodeNext );

   pNode->m_key.~KEY_TYPE();
   pNode->m_value.~VAL_TYPE();

   m_nCount --;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
POSITION tMapRep<KEY_TYPE,VAL_TYPE>::Lookup( const KEY_TYPE& key ) const
{
   if ( m_nCount < 1 ) return null;
   if ( m_ppHashTable == null ) return null;
   if ( m_nHashTableSize < 1 ) return null;

   tNode* pNodePrev;
   unsigned int uiIndex, uiHashKey;
   return ( POSITION ) FindKey( key, pNodePrev, uiIndex, uiHashKey );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
POSITION tMapRep<KEY_TYPE,VAL_TYPE>::GetFirst() const
{
   if ( m_nCount < 1 ) return null;
   if ( m_ppHashTable == null ) return null;

   tNode* pNode;
   for ( unsigned int uiIndex = 0; uiIndex < m_nHashTableSize; uiIndex ++ ) {
      if ( (pNode = m_ppHashTable[ uiIndex ]) != null )
         return ( POSITION ) pNode;
   }
   return null;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
POSITION tMapRep<KEY_TYPE,VAL_TYPE>::GetNext( POSITION pos ) const
{
   if ( pos == null ) return GetFirst();

   tNode* pNode = ( ( tNode* ) pos )->m_pNodeNext;
   if ( pNode ) return ( POSITION ) pNode;

   if ( m_ppHashTable == null ) return null;

   unsigned int uiIndex = ( ( tNode* ) pos )->m_uiHashTableIndex+1;

   for ( ; uiIndex < m_nHashTableSize; uiIndex ++ ) {
      if ( ( pNode = m_ppHashTable[ uiIndex ] ) != null )
         return ( POSITION ) pNode;
   }
   return null;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
void tMapRep<KEY_TYPE,VAL_TYPE>::SetAt( const KEY_TYPE& key, const VAL_TYPE& value )
{
   if ( m_ppHashTable == null ) {
      if ( m_nHashTableSize < 1 )
         return;

      if ( ( m_ppHashTable = new tNode*[ m_nHashTableSize ] ) == null )
         return;

      memset( m_ppHashTable, 0, m_nHashTableSize * sizeof( tNode* ) );
   }

   tNode* pNodePrev;
   unsigned int uiIndex, uiHashKey;
   tNode* pNode = FindKey( key, pNodePrev, uiIndex, uiHashKey );

   if ( pNode ) {
      pNode->m_value = value;
      return;
   }

   SetNewNodeAt( uiIndex, uiHashKey, key, value );

   // If load factor is exceeded, rehash automatically.
   if ( m_nCount > m_nMaxCount )
      Rehash();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline const KEY_TYPE& tMapRep<KEY_TYPE,VAL_TYPE>::GetKeyAt( POSITION pos ) const
{
   return ( ( tNode* ) pos )->m_key;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline const VAL_TYPE& tMapRep<KEY_TYPE,VAL_TYPE>::GetValueAt( POSITION pos ) const
{
   return ( ( tNode* ) pos )->m_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline VAL_TYPE& tMapRep<KEY_TYPE,VAL_TYPE>::GetValueAt( POSITION pos )
{
   return ( ( tNode* ) pos )->m_value;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline tMapRep<KEY_TYPE,VAL_TYPE>::tValueProxy tMapRep<KEY_TYPE,VAL_TYPE>::operator[]( const KEY_TYPE& key ) const
{
   return tMapRep<KEY_TYPE,VAL_TYPE>::tValueProxy( (tMapRep<KEY_TYPE,VAL_TYPE>&)(*this), (KEY_TYPE&)(key) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline tMapRep<KEY_TYPE,VAL_TYPE>::tValueProxy tMapRep<KEY_TYPE,VAL_TYPE>::operator[]( const KEY_TYPE& key )
{
   return tMapRep<KEY_TYPE,VAL_TYPE>::tValueProxy( *this, (KEY_TYPE&)(key) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
bool tMapRep<KEY_TYPE,VAL_TYPE>::RemoveKey( const KEY_TYPE& key )
{
   if ( m_nCount < 1 ) return false;
   if ( m_ppHashTable == null ) return false;
   if ( m_nHashTableSize < 1 ) return false;


   tNode* pNodePrev;
   unsigned int uiIndex, uiHashKey;
   tNode* pNode = FindKey( key, pNodePrev, uiIndex, uiHashKey );
   
   // Key found.
   if ( pNode ) {
      tNode* pNodeNext = pNode->m_pNodeNext;
      if ( pNodePrev )
         pNodePrev->m_pNodeNext = pNodeNext;
      else
         m_ppHashTable[ uiIndex ] = pNodeNext;

      DeleteNode( pNode );
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
void tMapRep<KEY_TYPE,VAL_TYPE>::RemoveAll()
{
   if ( m_ppHashTable ) {
      for ( unsigned uiIndex = 0; uiIndex < m_nHashTableSize && m_nCount > 0; uiIndex ++ ) {
         tNode* pNode = m_ppHashTable[ uiIndex ];
         if ( pNode == null ) continue;

         DestructList( pNode );
      }
   }

    // free hash table
    if ( m_ppHashTable )
      delete [] m_ppHashTable;
    m_ppHashTable = null;

    m_nCount = 0;
    m_pNextAvailNode = null;
    BlockFree( m_pBlocks );
    m_pBlocks = null;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline unsigned int tMapRep<KEY_TYPE,VAL_TYPE>::GetHashTableSize() const
{
   return m_nHashTableSize;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline unsigned int tMapRep<KEY_TYPE,VAL_TYPE>::GetCount() const
{
   return m_nCount;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
tMapRep<KEY_TYPE,VAL_TYPE>& tMapRep<KEY_TYPE,VAL_TYPE>::operator=( const tMapRep<KEY_TYPE,VAL_TYPE>& map )
{
   if ( this != &map ) {
      RemoveAll();

      m_nCount          = 0;
      m_nHashTableSize  = map.m_nHashTableSize;
      m_nMaxCount       = map.m_nMaxCount;
      m_ppHashTable     = new tNode*[ map.m_nHashTableSize ];
      m_nBlockSize      = map.m_nBlockSize;
      m_pNextAvailNode  = null;
      m_pBlocks         = null;
      m_cbNode          = map.m_cbNode;
      m_cbBlock         = map.m_cbBlock;

      memset( m_ppHashTable, 0, m_nHashTableSize * sizeof(tNode*) );

      for ( POSITION pos = map.GetFirst(); pos != null && m_nCount < map.m_nCount; pos = map.GetNext( pos ), m_nCount++ )
         SetNewNodeAt( ( ( tNode* ) pos )->m_uiHashTableIndex,
                             ( ( tNode* ) pos )->m_uiHashKey,
                             ( ( tNode* ) pos )->m_key,
                             ( ( tNode* ) pos )->m_value );
   }
   return *this;
}

#undef   MUTABLE

#endif //_tMapRep_h_
