/*
 *
 * tList.h
 *
 */

#ifndef  _tList_h_
#define  _tList_h_

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _tCountedReference_h_
   #include "tcountedreference.h"
#endif
#ifndef  _tListRep_h_
   #include "tlistrep.h"
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
// CLASS: tList< ELT_TYPE > //////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE> class tList : private tCountedReference< tListRep<ELT_TYPE> > {
private:

   enum { Block_Size = 32 };              // Default Block Size.

public:

#if !defined(M_AIX) && !defined(M_GNU)
   // generated (by modern compilers):
   //
   // tList( const tList& )
   //~tList()
   // tList& operator=( const tList& )
#else
   tList( const tList<ELT_TYPE>& ref )
      : tCountedReference< tListRep<ELT_TYPE> >( (tCountedReference< tListRep<ELT_TYPE> >&) ref ) {}
  ~tList() {}
   tList<ELT_TYPE>& operator=( const tList<ELT_TYPE>& ref ) {
      if ( this != &ref )
         tCountedReference< tListRep<ELT_TYPE> >::operator =( (tCountedReference< tListRep<ELT_TYPE> >&) ref );
      return *this;
   }
#endif

                     tList                ( unsigned int nBlockSize = Block_Size );
                     
   POSITION          GetHead              () const;
   POSITION          GetTail              () const;
   POSITION          GetNext              ( POSITION pos ) const;
   POSITION          GetPrev              ( POSITION pos ) const;
   const ELT_TYPE&   GetAt                ( POSITION pos ) const;

   bool              Sort                 ();
   void              RemoveAt             ( POSITION pos );
   void              RemoveAll            ();
                                          
   POSITION          AddTail              ( const ELT_TYPE& elt );
   POSITION          AddHead              ( const ELT_TYPE& elt );
   POSITION          InsertBefore         ( POSITION pos, const ELT_TYPE& elt );
   POSITION          InsertAfter          ( POSITION pos, const ELT_TYPE& elt );
                                     
   POSITION          AppendTail           ( const tList<ELT_TYPE>& list );
   POSITION          AppendHead           ( const tList<ELT_TYPE>& list );
   POSITION          AppendBefore         ( POSITION pos, const tList<ELT_TYPE>& list );
   POSITION          AppendAfter          ( POSITION pos, const tList<ELT_TYPE>& list );
                                          
   unsigned int      GetCount             ();

   POSITION          Search               ( const ELT_TYPE& elt );
                                          
   ELT_TYPE&         operator[]           ( POSITION pos );
   const ELT_TYPE&   operator[]           ( POSITION pos ) const;
};


//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline tList<ELT_TYPE>::tList( unsigned int nBlockSize /*= Block_Size*/ )
:  tCountedReference< tListRep<ELT_TYPE> >( tListRep<ELT_TYPE>( nBlockSize ) )
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::GetHead() const
{
   return (*this)->GetHead();
}
   
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::GetTail() const
{
   return (*this)->GetTail();
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::GetNext( POSITION pos ) const
{
   return (*this)->GetNext( pos );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::GetPrev( POSITION pos ) const
{
   return (*this)->GetPrev( pos );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline const ELT_TYPE& tList<ELT_TYPE>::GetAt( POSITION pos ) const
{
   return (*this)->GetAt( pos );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline bool tList<ELT_TYPE>::Sort()
{
   return (*this)->Sort();
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline void tList<ELT_TYPE>::RemoveAt( POSITION pos )
{
   (*this)->RemoveAt( pos );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline void tList<ELT_TYPE>::RemoveAll()
{
   (*this)->RemoveAll();
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::AddTail( const ELT_TYPE& elt )
{
   return (*this)->AddTail( elt );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::AddHead( const ELT_TYPE& elt )
{
   return (*this)->AddHead( elt );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::InsertBefore( POSITION pos, const ELT_TYPE& elt )
{
   return (*this)->InsertBefore( pos, elt );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::InsertAfter( POSITION pos, const ELT_TYPE& elt )
{
   return (*this)->InsertAfter( pos, elt );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::AppendTail( const tList<ELT_TYPE>& list )
{
   return (*this)->AppendTail( *list );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::AppendHead( const tList<ELT_TYPE>& list )
{
   return (*this)->AppendHead( *list );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::AppendBefore( POSITION pos, const tList<ELT_TYPE>& list )
{
   return (*this)->AppendBefore( pos, *list );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::AppendAfter( POSITION pos, const tList<ELT_TYPE>& list )
{
   return (*this)->AppendAfter( pos, *list );
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline unsigned int tList<ELT_TYPE>::GetCount()
{
   return (*this)->GetCount();
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tList<ELT_TYPE>::Search( const ELT_TYPE& elt )
{
   return (*this)->Search( elt );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline ELT_TYPE& tList<ELT_TYPE>::operator[]( POSITION pos )
{
   StopSharing();
   return operator*()[ pos ];
}
 
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline const ELT_TYPE& tList<ELT_TYPE>::operator[]( POSITION pos ) const
{
   return operator*()[ pos ];
}

#endif //_tList_h_
