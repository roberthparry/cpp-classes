/*
 *
 * tListRep.h
 *
 */

#ifndef  _tListRep_h_
#define  _tListRep_h_

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef  _INC_NEW
   #include <new.h>
#endif
#ifndef  _INC_STRING
   #include <string.h>
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef null
#define null 0
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef  POSITION_DEFINED
   #define  POSITION_DEFINED
   typedef struct _POSITION {} *POSITION;
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
// CLASS: tListRep< ELT_TYPE > ///////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE> class tListRep {
private:

   ///////////////////////////////////////////////////////////////////////////////////////////////
   // CLASS: tNode ///////////////////////////////////////////////////////////////////////////////
   class tNode {
   public:
      tNode*         m_pPrev;
      tNode*         m_pNext;
      ELT_TYPE*      m_pItem;             // Use a pointer to optimise sorting
   };

   ///////////////////////////////////////////////////////////////////////////////////////////////
   // STRUCT: tBlock /////////////////////////////////////////////////////////////////////////////
   struct tBlock {
      tBlock*        m_pBlockNext;        // link to next tBlock.
      /*unsigned char* m_abNodeHeap[];*/  // m_nBlockSize nodes.
   };

   tNode*            m_pListHead;         // front node in the list.
   tNode*            m_pListTail;         // back node in the list.
   unsigned int      m_nCount;            // number of items in list.

   // Use tListRep::NewNode to get the next available node to use and tListRep::DeleteNode to dispose.
   // A tBlock is a structure with a pointer to the next structure. Allocation will allocate
   // one tBlock structure plus nBlockSize tListRep::tNodes plus space for the m_pItem.
   unsigned int      m_nBlockSize;        // Default is 32.
   tNode*            m_pNextAvailNode;    // Next available slot for new tNode.
   tBlock*           m_pBlocks;           // The chained Blocks.

   // cached data
   unsigned  m_cbNode;            // memory to allocate for node & element
   unsigned  m_cbBlock;           // memory to allocate for whole block

private:

   tNode*            NewNode              ( tNode* pPrev, tNode* pNext, const ELT_TYPE& elt );
   void              DeleteNode           ( tNode* pNode );
   void              BlockFree            ( tBlock* pBlocks );

   static bool       QuickSort            ( tNode* pFirst, tNode* pLast );

public:

                    ~tListRep             ();
                     tListRep             ( unsigned int nBlockSize = 32 );
                     tListRep             ( const tListRep<ELT_TYPE>& list );
   POSITION          GetHead              () const;
   POSITION          GetTail              () const;
   POSITION          GetNext              ( POSITION pos ) const;
   POSITION          GetPrev              ( POSITION pos ) const;
   const ELT_TYPE&   GetAt                ( POSITION pos ) const;

   bool              Sort                 ();
   void              RemoveAt             ( POSITION pos );
   void              RemoveAll            ();
                                          
   POSITION          AddTail              ( const ELT_TYPE& elt );
   POSITION          AddHead              ( const ELT_TYPE& elt );
   POSITION          InsertBefore         ( POSITION pos, const ELT_TYPE& elt );
   POSITION          InsertAfter          ( POSITION pos, const ELT_TYPE& elt );
                                     
   POSITION          AppendTail           ( const tListRep<ELT_TYPE>& list );
   POSITION          AppendHead           ( const tListRep<ELT_TYPE>& list );
   POSITION          AppendBefore         ( POSITION pos, const tListRep<ELT_TYPE>& list );
   POSITION          AppendAfter          ( POSITION pos, const tListRep<ELT_TYPE>& list );
                                          
   unsigned int      GetCount             ();

   POSITION          Search               ( const ELT_TYPE& elt ) const;
                                          
   tListRep<ELT_TYPE>&         operator=            ( const tListRep<ELT_TYPE>& list );
   ELT_TYPE&         operator[]           ( POSITION pos );
   const ELT_TYPE&   operator[]           ( POSITION pos ) const;
};

#ifndef  COMPAREELEMENTS_DEFINED
#define  COMPAREELEMENTS_DEFINED
//////////////////////////////////////////////////////////////////////////////////////////////////
inline int CompareElements( const char*& psz1, const char*& psz2 )
{
   return strcmp( psz1, psz2 );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline int CompareElements( const ELT_TYPE& elt1, const ELT_TYPE& elt2 )
{
   return ( elt1 == elt2 ) ? 0 : ( ( elt1 < elt2 ) ? -1 : 1 );
}

#endif //COMPAREELEMENTS_DEFINED

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline tListRep<ELT_TYPE>::~tListRep()
{
   RemoveAll();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline tListRep<ELT_TYPE>::tListRep( unsigned int nBlockSize )
:  m_nCount         ( 0 )
,  m_pListHead      ( null )
,  m_pListTail      ( null )
,  m_nBlockSize     ( nBlockSize )
,  m_pNextAvailNode ( null )
,  m_pBlocks        ( null )
,  m_cbNode         ( sizeof( tNode ) + sizeof( ELT_TYPE ) )
,  m_cbBlock        ( sizeof( tBlock ) + m_nBlockSize * m_cbNode )
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline tListRep<ELT_TYPE>::tListRep( const tListRep<ELT_TYPE>& list )
:  m_nCount         ( 0 )
,  m_pListHead      ( null )
,  m_pListTail      ( null )
,  m_nBlockSize     ( list.m_nBlockSize )
,  m_pNextAvailNode ( null )
,  m_pBlocks        ( null )
,  m_cbNode         ( list.m_cbNode )
,  m_cbBlock        ( list.m_cbBlock )
{
   for ( POSITION pos = list.GetHead();
         pos != null;
         pos = list.GetNext( pos ) )
      AddTail( list.GetAt( pos ) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
tListRep<ELT_TYPE>::tNode* tListRep<ELT_TYPE>::NewNode( tListRep<ELT_TYPE>::tNode* pPrev,
                                                        tListRep<ELT_TYPE>::tNode* pNext,
                                                        const ELT_TYPE& elt )
{
   tNode* pNode;

   if ( m_pNextAvailNode == null ) {
      tBlock* pBlock = ( tBlock* ) malloc( m_cbBlock );
      ::new( ( void* ) pBlock ) tBlock();

      // chain the memory blocks together.
      pBlock->m_pBlockNext = m_pBlocks;
      m_pBlocks = pBlock;

      unsigned char* pbNodeHeap = ( unsigned char* )( pBlock + 1 );
      pbNodeHeap += ( m_nBlockSize - 1 ) * m_cbNode;

      for ( int iBlock = (int) m_nBlockSize - 1; iBlock >= 0; iBlock--, pbNodeHeap -= m_cbNode ) {
         pNode = ( tNode* ) pbNodeHeap;
         ::new( ( void* ) pNode ) tNode();
         pNode->m_pNext = m_pNextAvailNode;
         pNode->m_pItem = ( ELT_TYPE* ) &pNode[ 1 ];
         m_pNextAvailNode = pNode;
      }
   }

   pNode = m_pNextAvailNode;
   m_pNextAvailNode = m_pNextAvailNode->m_pNext;

   pNode->m_pPrev = pPrev;
   pNode->m_pNext = pNext;
   ::new( ( void* ) pNode->m_pItem ) ELT_TYPE( elt );

   m_nCount ++;

   return pNode;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
void tListRep<ELT_TYPE>::DeleteNode( tListRep<ELT_TYPE>::tNode* pNode )
{
   pNode->m_pItem->~ELT_TYPE();
   pNode->m_pNext = m_pNextAvailNode;
   m_pNextAvailNode = pNode;
   m_nCount --;

   if ( m_nCount == 0 )
      RemoveAll();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
void tListRep<ELT_TYPE>::BlockFree( tListRep<ELT_TYPE>::tBlock* pBlocks )
{
   if ( pBlocks ) {
      BlockFree( pBlocks->m_pBlockNext );
      free( pBlocks );
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tListRep<ELT_TYPE>::GetHead() const
{
   return ( POSITION ) m_pListHead;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tListRep<ELT_TYPE>::GetTail() const
{
   return ( POSITION ) m_pListTail;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tListRep<ELT_TYPE>::GetNext( POSITION pos ) const
{
   return ( POSITION ) ( ( tNode* ) pos )->m_pNext;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline POSITION tListRep<ELT_TYPE>::GetPrev( POSITION pos ) const
{
   return ( POSITION ) ( ( tNode* ) pos )->m_pPrev;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline const ELT_TYPE& tListRep<ELT_TYPE>::GetAt( POSITION pos ) const
{
   return *( ( tNode* ) pos )->m_pItem;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
bool tListRep<ELT_TYPE>::QuickSort( tNode* pFirst, tNode* pLast )
{
   if ( pFirst == pLast )
      return true;

   tNode NodeTemp;
   NodeTemp.m_pNext = pFirst;

   tNode* pLeft = &NodeTemp;
   tNode* pRight = pLast;

   for ( ; ; ) {
      while ( CompareElements( *( pLeft = pLeft->m_pNext )->m_pItem, *pLast->m_pItem ) < 0 );

      while ( pRight != pLeft ) {
         if ( CompareElements( *( pRight = pRight->m_pPrev )->m_pItem, *pLast->m_pItem ) <= 0 )
            break;
      }

      if ( pRight == pLeft )
         break;

      NodeTemp.m_pItem = pLeft->m_pItem;
      pLeft->m_pItem = pRight->m_pItem;
      pRight->m_pItem = NodeTemp.m_pItem;
   }

   NodeTemp.m_pItem = pLeft->m_pItem;
   pLeft->m_pItem = pLast->m_pItem;
   pLast->m_pItem = NodeTemp.m_pItem;

   if ( pLeft != pFirst ) {
      if ( ! tListRep<ELT_TYPE>::QuickSort( pFirst, pLeft->m_pPrev ) )
         return false;
   }

   if ( pRight != pLast ) {
      if ( ! tListRep<ELT_TYPE>::QuickSort( pRight->m_pNext, pLast ) )
         return false;
   }

   return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline bool tListRep<ELT_TYPE>::Sort()
{
   if ( m_nCount < 2 )
      return true;

   return tListRep<ELT_TYPE>::QuickSort( m_pListHead, m_pListTail );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
void tListRep<ELT_TYPE>::RemoveAt( POSITION pos )
{
   if ( pos == null )
      return;

   tNode* pListNodePrev = ( ( tNode* ) pos )->m_pPrev;
   tNode* pListNodeNext = ( ( tNode* ) pos )->m_pNext;
   if ( pListNodePrev ) pListNodePrev->m_pNext = pListNodeNext;
   if ( pListNodeNext ) pListNodeNext->m_pPrev = pListNodePrev;

   if ( pListNodeNext == null )
      m_pListTail = pListNodePrev;
   if ( pListNodePrev == null )
      m_pListHead = pListNodeNext;

   DeleteNode( ( tNode* ) pos );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
void tListRep<ELT_TYPE>::RemoveAll()
{
   for ( tNode* pListNode = m_pListHead; pListNode != null; pListNode = pListNode->m_pNext )
      pListNode->m_pItem->~ELT_TYPE();

	BlockFree( m_pBlocks );
   m_nCount = 0;
   m_pListHead = null;
   m_pListTail = null;
	m_pBlocks = null;
   m_pNextAvailNode = null;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
POSITION tListRep<ELT_TYPE>::AddTail( const ELT_TYPE& elt )
{
   tNode* pOldTail = m_pListTail;
   tNode* pNewTail = NewNode( pOldTail, null, elt );
   if ( pNewTail == null ) return null;

   if ( pOldTail != null )
      pOldTail->m_pNext = pNewTail;

   m_pListTail = pNewTail;
   if ( m_nCount == 1 )
      m_pListHead = pNewTail;

   return ( POSITION ) pNewTail;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
POSITION tListRep<ELT_TYPE>::AddHead( const ELT_TYPE& elt )
{
   tNode* pOldHead = m_pListHead;
   tNode* pNewHead = NewNode( null, pOldHead, elt );
   if ( pNewHead == null ) return null;

   if ( pOldHead )
      pOldHead->m_pPrev = pNewHead;

   m_pListHead = pNewHead;
   if ( m_nCount == 1 )
      m_pListTail = pNewHead;

   return ( POSITION ) pNewHead;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
POSITION tListRep<ELT_TYPE>::InsertBefore( POSITION pos, const ELT_TYPE& elt )
{
   if ( pos == null )
      return null;

   tNode* pPrevItem = ( ( tNode* ) pos )->m_pPrev;
   tNode* pNewItem = NewNode( pPrevItem, ( tNode* ) pos, elt );
   if ( pNewItem == null ) return null;

   if ( pPrevItem ) pPrevItem->m_pNext = pNewItem;
   ( ( tNode* ) pos )->m_pPrev = pNewItem;

   if ( pNewItem->m_pPrev == null )
      m_pListHead = pNewItem;

   return ( POSITION ) pNewItem;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
POSITION tListRep<ELT_TYPE>::InsertAfter( POSITION pos, const ELT_TYPE& elt )
{
   if ( pos == null )
      return null;

   tNode* pNextItem = ( ( tNode* ) pos )->m_pNext;
   tNode* pNewItem = NewNode( ( tNode* ) pos, pNextItem, elt );
   if ( pNewItem == null ) return null;

   if ( pNextItem ) pNextItem->m_pPrev = pNewItem;
   ( ( tNode* ) pos )->m_pNext = pNewItem;

   if ( pNewItem->m_pNext == null )
      m_pListTail = pNewItem;

   return ( POSITION ) pNewItem;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
POSITION tListRep<ELT_TYPE>::AppendTail( const tListRep<ELT_TYPE>& list )
{
   unsigned int nCount = list.m_nCount;
   if ( nCount < 1 ) return ( POSITION ) m_pListTail;

   tNode* pListNodeSrc = list.m_pListHead;

   // The counter caters for appending a list to itself.
   for ( unsigned int ui = 0; ui < nCount && pListNodeSrc; ui ++ ) {
      AddTail( *pListNodeSrc->m_pItem );
      pListNodeSrc = pListNodeSrc->m_pNext;
   }
   return ( POSITION ) m_pListTail;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
POSITION tListRep<ELT_TYPE>::AppendHead( const tListRep<ELT_TYPE>& list )
{
   unsigned int nCount = list.m_nCount;
   if ( nCount < 1 ) return ( POSITION ) m_pListHead;

   tNode* pListNodeSrc = list.m_pListTail;

   // The counter caters for appending a list to itself.
   for ( unsigned int ui = 0; ui < nCount && pListNodeSrc; ui ++ ) {
      AddHead( *(pListNodeSrc->m_pItem) );
      pListNodeSrc = pListNodeSrc->m_pPrev;
   }
   return ( POSITION ) m_pListHead;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
POSITION tListRep<ELT_TYPE>::AppendBefore( POSITION pos, const tListRep<ELT_TYPE>& list )
{
   unsigned int nCount = list.m_nCount;
   if ( nCount < 1 ) return pos;

   tNode* pListNodeSrc = list.m_pListTail;

   // The counter caters for appending a list to itself.
   for ( unsigned int ui = 0; ui < nCount && pListNodeSrc; ui ++ ) {
      pos = InsertBefore( pos, *(pListNodeSrc->m_pItem) );
      pListNodeSrc = pListNodeSrc->m_pPrev;
   }
   return pos;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
POSITION tListRep<ELT_TYPE>::AppendAfter( POSITION pos, const tListRep<ELT_TYPE>& list )
{
   unsigned int nCount = list.m_nCount;
   if ( nCount < 1 ) return pos;

   tNode* pListNodeSrc = list.m_pListHead;

   // The counter caters for appending a list to itself.
   for ( unsigned int ui = 0; ui < nCount && pListNodeSrc; ui ++ ) {
      pos = InsertAfter( pos, *(pListNodeSrc->m_pItem) );
      pListNodeSrc = pListNodeSrc->m_pNext;
   }
   return pos;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline unsigned int tListRep<ELT_TYPE>::GetCount()
{
   return m_nCount;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
POSITION tListRep<ELT_TYPE>::Search( const ELT_TYPE& elt ) const
{
   for ( tNode* pNode = m_pListHead; pNode != null; pNode = pNode->m_pNext ) {
      if ( CompareElements( elt, *pNode->m_pItem ) == 0 )
         return ( POSITION ) pNode;
   }
   return null;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline tListRep<ELT_TYPE>& tListRep<ELT_TYPE>::operator=( const tListRep<ELT_TYPE>& list )
{
   if ( this != &list ) {
      RemoveAll();
      AppendTail( list );
   }
   return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline ELT_TYPE& tListRep<ELT_TYPE>::operator[]( POSITION pos )
{
   return *( ( tNode* ) pos )->m_pItem;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
const ELT_TYPE& tListRep<ELT_TYPE>::operator[]( POSITION pos ) const
{
   return *( ( tNode* ) pos )->m_pItem;
}

#endif //_tListRep_h_