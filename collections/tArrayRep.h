/*
 *
 * tArrayRep.h
 *
 */

#ifndef  _tArrayRep_h_
#define  _tArrayRep_h_

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef  _INC_STRING
   #include <string.h>
#endif
#ifndef  __NEW_H
   #include <new.h>
#endif
#if !defined( __BORLANDC__ )
   #ifndef _INC_MALLOC
      #include <malloc.h>
   #endif
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef null
#define null 0
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
// CLASS: tArrayRep< ELT_TYPE > //////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
template <class ELT_TYPE> class tArrayRep {

private:

   ELT_TYPE*         m_pItems;            // Address of items in the tArrayRep.
   unsigned int      m_nItems;            // Actual items in the tArrayRep.
   unsigned int      m_nAllocatedItems;   // Allocated items in the tArrayRep.
   unsigned int      m_nBlockSize;        // Allocation jump increment.

private:

   static bool       QuickSort            ( ELT_TYPE* pItems, int iLeft, int iRight );

public:

                    ~tArrayRep            ();
                     tArrayRep            ( unsigned nBlockSize = 32 );
                     tArrayRep            ( const tArrayRep<ELT_TYPE>& array );


   void              SetAt                ( unsigned int uiIndex, const ELT_TYPE& elt );
   ELT_TYPE&         GetAt                ( unsigned int uiIndex ) const;
   unsigned int      Add                  ( const ELT_TYPE& elt );
   unsigned int      Append               ( const tArrayRep<ELT_TYPE>& array );
   void              SetSize              ( unsigned int nItems );
   unsigned int      GetSize              () const;
   bool              Sort                 ();
   void              RemoveAll            ();

   unsigned int      LinearSearch         ( const ELT_TYPE& elt ) const;
   unsigned int      BinarySearch         ( const ELT_TYPE& elt ) const;

   tArrayRep<ELT_TYPE>& operator=         ( const tArrayRep<ELT_TYPE>& array );
   ELT_TYPE&            operator[]        ( unsigned int uiIndex );
   const ELT_TYPE&      operator[]        ( unsigned int uiIndex ) const;
};

#ifndef  COMPAREELEMENTS_DEFINED
#define  COMPAREELEMENTS_DEFINED
//////////////////////////////////////////////////////////////////////////////////////////////////
inline int CompareElements( const char*& psz1, const char*& psz2 )
{
   return strcmp( psz1, psz2 );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline int CompareElements( const ELT_TYPE& elt1, const ELT_TYPE& elt2 )
{
   return ( elt1 == elt2 ) ? 0 : ( ( elt1 < elt2 ) ? -1 : 1 );
}

#endif //COMPAREELEMENTS_DEFINED

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline tArrayRep<ELT_TYPE>::~tArrayRep()
{
   RemoveAll();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline tArrayRep<ELT_TYPE>::tArrayRep( unsigned nBlockSize /*= Block_Size*/ )
:  m_nBlockSize      ( nBlockSize )
,  m_nAllocatedItems ( 0 )
,  m_nItems          ( 0 )
,  m_pItems          ( null )
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
tArrayRep<ELT_TYPE>::tArrayRep( const tArrayRep<ELT_TYPE>& array )
:  m_nBlockSize      ( array.m_nBlockSize )
,  m_nAllocatedItems ( array.m_nAllocatedItems )
,  m_nItems          ( array.m_nItems )
{
   if ( m_nAllocatedItems < 1 ) {
      m_pItems = null;
      return;
   }

   m_pItems = ( ELT_TYPE* ) malloc( m_nAllocatedItems * sizeof( ELT_TYPE ) );
   if ( m_pItems == null ) {
      RemoveAll();
      return;
   }

   ELT_TYPE* pItemDest = m_pItems;
   ELT_TYPE* pItemSrc  = array.m_pItems;

   for ( unsigned int uiIndex = 0; uiIndex < m_nItems; uiIndex ++ ) {
		::new( ( void* )pItemDest ) ELT_TYPE( *pItemSrc );
      pItemDest++;
      pItemSrc++;
   }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
void tArrayRep<ELT_TYPE>::SetAt( unsigned int uiIndex, const ELT_TYPE& elt )
{
   unsigned int nRequiredSize = uiIndex + 1;
   
   if ( m_nItems < nRequiredSize )
      SetSize( nRequiredSize );

   m_pItems[ uiIndex ] = elt;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline ELT_TYPE& tArrayRep<ELT_TYPE>::GetAt( unsigned int uiIndex ) const
{
   return m_pItems[ uiIndex ];
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
unsigned int tArrayRep<ELT_TYPE>::Add( const ELT_TYPE& elt )
{
   if ( m_nAllocatedItems <= m_nItems ) {
      m_nAllocatedItems += m_nBlockSize;
      m_pItems = ( ELT_TYPE* ) realloc( m_pItems, m_nAllocatedItems * sizeof( ELT_TYPE ) );
      if ( m_pItems == null ) {
         RemoveAll();
         return m_nItems;
      }
   }
   unsigned int uiIndex = m_nItems;

   ::new( ( void* ) ( m_pItems + uiIndex ) ) ELT_TYPE( elt );

   m_nItems ++;
   return uiIndex;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
unsigned int tArrayRep<ELT_TYPE>::Append( const tArrayRep<ELT_TYPE>& array )
{
   if ( array.m_nItems < 1 )
      return array.m_nItems - 1;

   unsigned int nItemsToAdd = array.m_nItems;
   unsigned int nTotalItems = m_nItems + nItemsToAdd;

   if ( nTotalItems >= m_nAllocatedItems ) {
      if ( nTotalItems < m_nAllocatedItems + m_nBlockSize )
         m_nAllocatedItems += m_nBlockSize;
      else
         m_nAllocatedItems = ( ( nTotalItems + m_nBlockSize - 1 ) / m_nBlockSize ) * m_nBlockSize;

      m_pItems = ( ELT_TYPE* ) realloc( m_pItems, m_nAllocatedItems * sizeof( ELT_TYPE ) );
      if ( m_pItems == null ) {
         RemoveAll();
         return m_nItems;
      }

   }

   ELT_TYPE* pItemDest = m_pItems + m_nItems;
   ELT_TYPE* pItemSrc  = array.m_pItems;

   for ( unsigned int uiIndex = 0; uiIndex < nItemsToAdd; uiIndex ++ ) {
		::new( ( void* )pItemDest ) ELT_TYPE( *pItemSrc );
      pItemDest ++;
      pItemSrc ++;
   }

   m_nItems = nTotalItems;
   return nTotalItems - 1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
void tArrayRep<ELT_TYPE>::SetSize( unsigned int nItems )
{
   if ( nItems == m_nItems )
      return;

   if ( nItems < 1 ) {
      RemoveAll();
      return;
   }

   if ( m_nItems > nItems ) {
      ELT_TYPE* pItem = &m_pItems[ nItems ];
      for ( unsigned int uiIndex = nItems; uiIndex < m_nItems; uiIndex ++ ) {
         pItem->~ELT_TYPE();
         pItem ++;
      }
   }
   
   if ( m_nAllocatedItems != nItems ) {
      if ( m_nAllocatedItems - m_nBlockSize > nItems || nItems >= m_nAllocatedItems )
         m_nAllocatedItems = ( ( nItems + m_nBlockSize - 1 ) / m_nBlockSize ) * m_nBlockSize;
      else
         m_nAllocatedItems += m_nBlockSize;

      m_pItems = ( ELT_TYPE* ) realloc( m_pItems, m_nAllocatedItems * sizeof( ELT_TYPE ) );
      if ( m_pItems == null ) {
         RemoveAll();
         return;
      }
   }

   if ( m_nItems < nItems ) {
      ELT_TYPE* pItem = &m_pItems[ m_nItems ];
      for ( unsigned int uiIndex = m_nItems; uiIndex < nItems; uiIndex ++ ) {
		   ::new( ( void* )pItem ) ELT_TYPE();
         pItem ++;
      }
   }

   m_nItems = nItems;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline unsigned int tArrayRep<ELT_TYPE>::GetSize() const
{
   return m_nItems;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
bool tArrayRep<ELT_TYPE>::QuickSort( ELT_TYPE* pItems, int iLeft, int iRight )
{
   if ( iRight <= iLeft )
      return true;

   int i = iLeft - 1;
   int j = iRight;
   
   ELT_TYPE itemTemp;

   for ( ; ; ) {
      while ( CompareElements( pItems[ ++i ], pItems[ iRight ] ) < 0 );

		while ( j > i ) {
         if ( CompareElements( pItems[ --j ], pItems[ iRight ] ) <= 0 )
            break;
      }
      if ( i >= j )
         break;

      itemTemp = pItems[ i ];
      pItems[ i ] = pItems[ j ];
      pItems[ j ] = itemTemp;
   }

   itemTemp = pItems[ i ];
   pItems[ i ] = pItems[ iRight ];
   pItems[ iRight ] = itemTemp;

   return tArrayRep<ELT_TYPE>::QuickSort( pItems, iLeft, i - 1 ) ?
      tArrayRep<ELT_TYPE>::QuickSort( pItems, i + 1, iRight ) : false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
bool tArrayRep<ELT_TYPE>::Sort()
{
	if ( m_nItems < 2 )
		return true;

   return tArrayRep<ELT_TYPE>::QuickSort( m_pItems, 0, ( int ) ( m_nItems - 1 ) );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
void tArrayRep<ELT_TYPE>::RemoveAll()
{
   if ( m_pItems ) {

      ELT_TYPE* pData = m_pItems;
      for ( unsigned int uiItem = 0; uiItem < m_nItems; uiItem ++ ) {
         pData->~ELT_TYPE();
         pData ++;
      }

      free( m_pItems );
      m_pItems = null;
   }

   m_nAllocatedItems = 0;
   m_nItems = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
unsigned int tArrayRep<ELT_TYPE>::LinearSearch( const ELT_TYPE& elt ) const
{
   for ( unsigned int uiItem = 0U; uiItem < m_nItems; uiItem ++ ) {
      if ( CompareElements( elt, m_pItems[ uiItem ] ) == 0 )
         return uiItem;
   }
   return UINT_MAX;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
unsigned int tArrayRep<ELT_TYPE>::BinarySearch( const ELT_TYPE& elt ) const
{
   long lFirstItem = 0L;
   long lLastItem  = ((long) m_nItems) - 1L;
   long lMiddleItem;

   int compare;

   while ( lFirstItem <= lLastItem ) {
      lMiddleItem = ( lFirstItem + lLastItem ) / 2L;

      if ( (compare = CompareElements( elt, m_pItems[ lMiddleItem ] )) < 0 )
         lLastItem = lMiddleItem - 1L;
      else if ( compare > 0 )
         lFirstItem = lMiddleItem + 1L;
      else
         return (unsigned int) lMiddleItem;
   }
  
   return UINT_MAX;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
tArrayRep<ELT_TYPE>& tArrayRep<ELT_TYPE>::operator=( const tArrayRep<ELT_TYPE>& array )
{
   if ( this != &array ) {
      RemoveAll();
      Append( array );
   }
   return *this;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline ELT_TYPE& tArrayRep<ELT_TYPE>::operator[]( unsigned int uiIndex )
{
   return m_pItems[ uiIndex ];
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline const ELT_TYPE& tArrayRep<ELT_TYPE>::operator[]( unsigned int uiIndex ) const
{
   return m_pItems[ uiIndex ];
}

#endif //_tArrayRep_h_
