/*
 *
 *	tCountedReference.h
 *
 */

#ifndef  _tCountedReference_h_
#define  _tCountedReference_h_


//////////////////////////////////////////////////////////////////////////////////////////////////
// Use this class to create reference counted objects.
template <class REFERENCED_TYPE>
class tCountedReference {
private:
   
   struct tValue {
      REFERENCED_TYPE   m_value;
      int               m_nReferences;
      bool              m_fShareable;

      tValue( ) : m_nReferences( 1 ), m_fShareable( true ) {}
      tValue( const REFERENCED_TYPE& value ) : m_value( value ), m_nReferences( 1 ), m_fShareable( true ) {}
   };

   tValue*  m_pValue;

private:

   void MakeUnique()
   {
      if ( m_pValue->m_nReferences > 1 ) {
         -- m_pValue->m_nReferences;
         m_pValue = new tValue( *m_pValue );
      }
   }

public:

   tCountedReference() : m_pValue( new tValue() ) {}

   tCountedReference( const tCountedReference<REFERENCED_TYPE>& countedref )
   {
      if ( countedref.m_pValue->m_fShareable ) {
         m_pValue = countedref.m_pValue;
         ++ m_pValue->m_nReferences;
      }
      else {
         m_pValue = new tValue( countedref.m_pValue->m_value );
      }
   }

   tCountedReference( const REFERENCED_TYPE& value )
    : m_pValue( new tValue( value ) ) {}

   tCountedReference<REFERENCED_TYPE>& operator=( const tCountedReference<REFERENCED_TYPE>& countedref )
   {
      if ( this != &countedref ) {
         if ( countedref.m_pValue->m_fShareable ) {
            ++ countedref.m_pValue->m_nReferences;
            if ( -- m_pValue->m_nReferences == 0 )
               delete m_pValue;

            m_pValue = countedref.m_pValue;
         }
         else {
            m_pValue = new tValue( countedref.m_pValue->m_value );
         }
      }
      return *this;
   }

  ~tCountedReference()
   {
      if ( -- m_pValue->m_nReferences == 0 )
         delete m_pValue;
   }

   //
   // mutating actions must all call MakeUnique() first.
   //

   // for calling mutating actions
   REFERENCED_TYPE* operator->()
   {
      MakeUnique();
      return &m_pValue->m_value;
   }

   REFERENCED_TYPE& operator*()
   {
      MakeUnique();
      return m_pValue->m_value;
   }

   // for calling non-mutating actions
   const REFERENCED_TYPE* operator->() const
   {
      return &m_pValue->m_value;
   }

   const REFERENCED_TYPE& operator*() const
   {
      return m_pValue->m_value;
   }

   // Note: in cases where components of REFERENCED_TYPE may be given to users to modify
   //       it is necessary to prevent the tCountedReference from being shared.
   //       otherwise references may be created to objects under change.
   //
   //       For example:
   //          int& tCountedReference< tArray<int> >::ElementAt( unsigned index )

   void StopSharing()
   {
      MakeUnique();
      m_pValue->m_fShareable = false;
   }
};

#endif //_tCountedReference_h_
