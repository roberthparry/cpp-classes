/*
 *
 * tArray.h
 *
 */

#ifndef  _tArray_h_
#define  _tArray_h_

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _tCountedReference_h_
   #include "tcountedreference.h"
#endif
#ifndef _tArrayRep_h_
   #include "tarrayrep.h"
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
// CLASS: tArray< ELT_TYPE > /////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
template <class ELT_TYPE> class tArray : private tCountedReference< tArrayRep<ELT_TYPE> > {
private:

   enum { Block_Size = 32 };

public:

#if !defined(M_AIX) && !defined(M_GNU)
   // generated (by modern compilers):
   //
   // tArray( const tArray& )
   //~tArray()
   // tArray& operator=( const tArray& )
#else
   tArray( const tArray<ELT_TYPE>& ref )
      : tCountedReference< tArrayRep<ELT_TYPE> >( (tCountedReference< tArrayRep<ELT_TYPE> >&) ref ) {}
  ~tArray() {}
   tArray<ELT_TYPE>& operator=( const tArray<ELT_TYPE>& ref ) {
      if ( this != &ref )
         tCountedReference< tArrayRep<ELT_TYPE> >::operator=( (tCountedReference< tArrayRep<ELT_TYPE> >&) ref );
      return *this;
   }
#endif

                     tArray               ( int nBlockSize = Block_Size );

   void              Add                  ( const ELT_TYPE& value );
   unsigned          Append               ( const tArray<ELT_TYPE>& array );
   void              SetSize              ( unsigned size );
   unsigned          GetSize              () const;
   void              Sort                 ();
   void              RemoveAll            ();

   unsigned int      LinearSearch         ( const ELT_TYPE& elt ) const;
   unsigned int      BinarySearch         ( const ELT_TYPE& elt ) const;
   
   const ELT_TYPE&   operator[]           ( unsigned index ) const;
   ELT_TYPE&         operator[]           ( unsigned index );
};

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline tArray<ELT_TYPE>::tArray( int nBlockSize /*= Block_Size*/ )
:  tCountedReference< tArrayRep<ELT_TYPE> >( tArrayRep<ELT_TYPE>( nBlockSize ) )
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline void tArray<ELT_TYPE>::Add( const ELT_TYPE& value )
{
   (*this)->Add( value );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline unsigned tArray<ELT_TYPE>::Append( const tArray<ELT_TYPE>& array )
{
   return (*this)->Append( *array );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline void tArray<ELT_TYPE>::SetSize( unsigned size )
{
   (*this)->SetSize( size );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline unsigned tArray<ELT_TYPE>::GetSize() const
{
   return (*this)->GetSize();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline void tArray<ELT_TYPE>::Sort()
{
   (*this)->Sort();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline void tArray<ELT_TYPE>::RemoveAll()
{
   (*this)->RemoveAll();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Search on unsorted array. Returns UINT_MAX if elt is not found.
template<class ELT_TYPE>
unsigned int tArray<ELT_TYPE>::LinearSearch( const ELT_TYPE& elt ) const
{
   return (*this)->LinearSearch( elt );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// Search on sorted array. Returns UINT_MAX if elt is not found.
template<class ELT_TYPE>
unsigned int tArray<ELT_TYPE>::BinarySearch( const ELT_TYPE& elt ) const
{
   return (*this)->BinarySearch( elt );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline const ELT_TYPE& tArray<ELT_TYPE>::operator[]( unsigned index ) const
{
   return operator*()[ index ];
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class ELT_TYPE>
inline ELT_TYPE& tArray<ELT_TYPE>::operator[]( unsigned index )
{
   StopSharing();
   return operator*()[ index ];
}

#endif //_tArray_h_
