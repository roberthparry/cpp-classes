/*
 *
 * tMap.h
 *
 */

#ifndef  _tMap_h_
#define  _tMap_h_

//////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef  _tMapRep_h_
   #include "tmaprep.h"
#endif
#ifndef  _tCountedReference_h_
   #include "tcountedreference.h"
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
// CLASS: tMap< KEY_TYPE, VAL_TYPE > /////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE> class tMap : private tCountedReference< tMapRep<KEY_TYPE,VAL_TYPE> > {
private:

   typedef tMapRep<KEY_TYPE,VAL_TYPE>::tValueProxy tMapValueProxy;

private:

   enum { Block_Size = 32 };  // Default Block Size.

public:

#if !defined(M_AIX) && !defined(M_GNU)
   // generated (by modern compilers):
   //
   // tMap( const tMap& )
   //~tMap()
   // tMap& operator=( const tMap& )
#else
   tMap( const tMap<KEY_TYPE,VAL_TYPE>& ref )
      : tCountedReference< tMapRep<KEY_TYPE,VAL_TYPE> >( (tCountedReference< tMapRep<KEY_TYPE,VAL_TYPE> >&) ref ) {}
  ~tMap() {}
   tMap<KEY_TYPE,VAL_TYPE>& operator=( const tMap<KEY_TYPE,VAL_TYPE>& ref ) {
      if ( this != &ref )
         tCountedReference< tMapRep<KEY_TYPE,VAL_TYPE> >::operator =( (tCountedReference< tMapRep<KEY_TYPE,VAL_TYPE> >&) ref );
      return *this;
   }
#endif

                     tMap                 ( unsigned int nBlockSize = Block_Size );
                                          
   void              InitHashTable        ( unsigned int nHashTableSize );
                                          
   POSITION          Lookup               ( const KEY_TYPE& key ) const;
   POSITION          GetFirst             () const;
   POSITION          GetNext              ( POSITION pos ) const;
                                          
   void              SetAt                ( const KEY_TYPE& key, const VAL_TYPE& value );
   const KEY_TYPE&   GetKeyAt             ( POSITION pos ) const;
   const VAL_TYPE&   GetValueAt           ( POSITION pos ) const;
   VAL_TYPE&         GetValueAt           ( POSITION pos );
   bool              RemoveKey            ( const KEY_TYPE& key );
   void              RemoveAll            ();
                                          
   unsigned int      GetHashTableSize     () const;
   unsigned int      GetCount             () const;
                                          
   tMapValueProxy    operator[]           ( const KEY_TYPE& key ) const;
   tMapValueProxy    operator[]           ( const KEY_TYPE& key );
};

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline tMap<KEY_TYPE,VAL_TYPE>::tMap( unsigned int nBlockSize /*= Block_Size*/ )
:  tCountedReference< tMapRep<KEY_TYPE,VAL_TYPE> >( tMapRep<KEY_TYPE,VAL_TYPE>( nBlockSize ) )
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline void tMap<KEY_TYPE,VAL_TYPE>::InitHashTable( unsigned int nHashTableSize )
{
   (*this)->InitHashTable( nHashTableSize );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline POSITION tMap<KEY_TYPE,VAL_TYPE>::Lookup( const KEY_TYPE& key ) const
{
   return (*this)->Lookup( key );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline POSITION tMap<KEY_TYPE,VAL_TYPE>::GetFirst() const
{
   return (*this)->GetFirst();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline POSITION tMap<KEY_TYPE,VAL_TYPE>::GetNext( POSITION pos ) const
{
   return (*this)->GetNext( pos );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline void tMap<KEY_TYPE,VAL_TYPE>::SetAt( const KEY_TYPE& key, const VAL_TYPE& value )
{
   (*this)->SetAt( key, value );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline const KEY_TYPE& tMap<KEY_TYPE,VAL_TYPE>::GetKeyAt( POSITION pos ) const
{
   return (*this)->GetKeyAt( pos );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline const VAL_TYPE& tMap<KEY_TYPE,VAL_TYPE>::GetValueAt( POSITION pos ) const
{
   return (*this)->GetValueAt( pos );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline VAL_TYPE& tMap<KEY_TYPE,VAL_TYPE>::GetValueAt( POSITION pos )
{
   StopSharing();
   return (*this)->GetValueAt( pos );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline bool tMap<KEY_TYPE,VAL_TYPE>::RemoveKey( const KEY_TYPE& key )
{
   return (*this)->RemoveKey( key );
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline void tMap<KEY_TYPE,VAL_TYPE>::RemoveAll()
{
   (*this)->RemoveAll();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline unsigned int tMap<KEY_TYPE,VAL_TYPE>::GetHashTableSize() const
{
   return (*this)->GetHashTableSize();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline unsigned int tMap<KEY_TYPE,VAL_TYPE>::GetCount() const
{
   return (*this)->GetCount();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline tMap<KEY_TYPE,VAL_TYPE>::tMapValueProxy tMap<KEY_TYPE,VAL_TYPE>::operator[]( const KEY_TYPE& key ) const
{
   return operator*()[ key ];
}

//////////////////////////////////////////////////////////////////////////////////////////////////
template<class KEY_TYPE, class VAL_TYPE>
inline tMap<KEY_TYPE,VAL_TYPE>::tMapValueProxy tMap<KEY_TYPE,VAL_TYPE>::operator[]( const KEY_TYPE& key )
{
   StopSharing();
   return operator*()[ key ];
}

#endif //_tMap_h_