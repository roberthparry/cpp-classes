/*
 *
 * tPrimes.h
 *
 */

#ifndef  _tPrimes_h_
#define  _tPrimes_h_

//////////////////////////////////////////////////////////////////////////////////////////////////
class tPrimes {
public:

   static unsigned int  KnuthBinaryGcd       ( unsigned int u, unsigned int v );
   static unsigned int  MulMod               ( unsigned int a, unsigned int b, unsigned int m );
   static unsigned int  PowMod               ( unsigned int a, unsigned int n, unsigned int m );
   static int           MillerRabinComposite ( unsigned int n, unsigned int b );
   static int           ProbablyPrime        ( unsigned int n );
   static unsigned int  NextPrime            ( unsigned int n );
};

#endif //_tPrimes_h_