/*
 *
 * tPrimes.cpp
 *
 */

#include <stdlib.h>

#include "tPrimes.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
unsigned int tPrimes::KnuthBinaryGcd( unsigned int u, unsigned int v )
{
   unsigned int k = 0;
   int t;

   // B1: Find power of 2
   while ( !((u | v) & 1) ) { u >>= 1; v >>= 1; k ++; }
   
   // B2: Initialise
   if ( u & 1 )
      { t = -(int)v; goto even_t; }
   else
      t = u;

   // B3: Halve t
halve_t:

   t >>= 1;

   // B4: Is t even?
even_t:
   if ( !(t & 1) ) goto halve_t;

   // B5: Reset max(u, v)
   if ( t > 0 )
      u = t;
   else
      v = -t;

   // B6: Subtract
   if ( (t = u - v) != 0 ) goto halve_t;

   return u << k;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
unsigned int tPrimes::MulMod( unsigned int a, unsigned int b, unsigned int m )
{
   if ( m == 0 )
      return a*b;

   unsigned int r = 0;
   while ( a ) {
      if ( a & 1 )
         if ( (r += b) > m ) r %= m;

      a >>= 1;
      if ( (b <<= 1) > m)
         b %= m;
   }

   return r;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
unsigned int tPrimes::PowMod( unsigned int a, unsigned int n, unsigned int m )
{
   unsigned int r;

   r = 1;
   while ( n ) {
      if ( n & 1 )
         r = MulMod( r, a, m );

      a = MulMod( a, a, m );
      n >>= 1;
   }
   return r;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int tPrimes::MillerRabinComposite( unsigned int n, unsigned int b )
{
   if ( !(n & 1) )
      return 1;

   if ( !(b %= n) )
      while ( (b = rand() + 2) >= n );

   if ( KnuthBinaryGcd( b, n ) > 1 )
      return 1;

   unsigned int q = n - 1;
   for ( unsigned int t = 0; !(q & 1); t ++ )
      q >>= 1;

   unsigned int r = PowMod( b, q, n);
   if ( r == 1 ) return 0;

   for ( unsigned int e = 0; e < t - 1; e ++ ) {
      if ( r == n - 1 ) break;
      r = MulMod( r, r, n );
   }

   return ( r == 1 || r == n - 1 ) ? 0 : 1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int tPrimes::ProbablyPrime( unsigned int n )
{
   unsigned int ab[] = { 31, 41, 59, 26, 53, 58, 97, 93, 23, 84, 62, 64, 33, 0 };

   for ( unsigned int* pb = ab; *pb; pb ++ ) {
      if ( MillerRabinComposite( n, *pb ) )
         return 0;
   }
   return 1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
unsigned int tPrimes::NextPrime( unsigned int n )
{
   if ( n < 2 )
      return 2;

   n += (n & 1) ? 2 : 1;

   unsigned int aui[] = { 3, 5, 7, 11, 13, 17, 0 };
   int fprime;

   do {
      fprime = 1;
      for ( unsigned int* pui = aui; *pui; pui ++ ) {

         if ( !(n % *pui) ) {
            if ( n == *pui )
               return n;

            fprime = 0;
            break;
         }
      }

      if ( fprime ) {
         if ( ( fprime = ProbablyPrime( n ) ) != 0 )
            return n;
         else
            n += 2;
      }
      else {
         n += 2;
      }

   } while ( ! fprime );

   return n;
}